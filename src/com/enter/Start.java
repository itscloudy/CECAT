package com.enter;

import java.io.IOException;

import com.view.Main;
/**
 * @author fuzhenyu
 * @软件名 中英指代注释工具 | CECAT(Chinese And English Coreference Annotation Tool)
 */
public class Start {
	//运行时jar的上级目录
	public static String parent_path;
	static {  
		parent_path=System.getProperties().getProperty("java.class.path");
		String classes[]=parent_path.split(";");
		if(classes.length>1)
			parent_path="";
		else {
			String levels[]=parent_path.replace('\\','~').split("~");
			parent_path="";
			for(int i=0;i<levels.length-1;i++) {
				parent_path+=levels[i]+"/";
			}
		}
	}
	public static void main(String[] args) throws IOException {
		Start.startApp();
	}
	
	public static void startApp() throws IOException{
		new Main();
	}
}
