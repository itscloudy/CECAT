package com.model;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.util.Defaults;
import com.view.Main;
import com.view.Right;

/**
 * 属性候选单元（创建并添加combox）
 * @author 柴家琪
 */
public class Attr {
	/**
	 * 因为复选框中有被选中的监听，监听在程序选择的（非手动）情况下也会被触发<br/>
	 * 为防止这种情况，程序选择之前会被上锁，程序选择完之后解锁
	 */
	public static boolean locked=false;
	private String key;
	private ArrayList<String> candidates=new ArrayList<>();
	private JComboBox<String> combox;
	private JPanel attrPanel;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public ArrayList<String> getCandidates() {
		return candidates;
	}
	public Attr(String key,String...candidates) {
		setKey(key);
		for(int i=0;i<candidates.length;i++) {
			this.candidates.add(candidates[i]);
		}
	}
	/**
	 * 获取value字符串
	 */
	public String getValueString() {
		String value="";
		Iterator<String> is=candidates.iterator();
		while(is.hasNext()) {
			value+=is.next();
			if(is.hasNext())
				value+=Main.dot;
		}
		return value;
	}
	/**
	 * 设置value字符集合
	 */
	public void setValueString(String value) {
		candidates.clear();
		String values[]=value.split(Main.dot);
		for(int i=0;i<values.length;i++) {
			candidates.add(values[i]);
		}
	}
	/**
	 * 获取候选词数组
	 */
	public String[] Candidates() {
		String c[]=new String[candidates.size()];
		int index=0;
		Iterator<String> is=candidates.iterator();
		while(is.hasNext()) {
			c[index]=is.next();
			index++;
		}
		return c;
	}
	/**
	 * 获取此属性的下拉选项框
	 */
	public JComboBox<String> getCombox(){
		return getCombox(false);
	}
	/**
	 * 获取Combox
	 * 
	 */
	private JComboBox<String> getCombox(boolean reDo) {
		if(combox==null||reDo) {
			combox=Defaults.combox(Candidates());//不存在就创建
			combox.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if(e.getStateChange()==ItemEvent.SELECTED&&!locked) {
						Node pre=Right.preShowing;
						if(pre!=null) {
							System.out.println(pre.getCellText(Right.preShowing.preShowing==1, 0));
							pre.modifyAttr(Right.preShowing.preShowing==Right.CN, key, (String)combox.getSelectedItem());
						}
					}
				}
			});
			combox.setForeground(Color.DARK_GRAY);
		}
		return combox;
	}
	/**
	 * 填充属性版
	 * @param reDo 是否重新加载属性（初始化和设置属性选项时为true)
	 */
	public void addToBearer(boolean reDo) {		
		attrPanel().removeAll();	//先移除所有组件
		destroy();
		//创建左边标签
		int l_width=(int)(Right.right_width*0.4);
		JLabel l=Defaults.label(getKey(),Color.DARK_GRAY,Defaults.defaultFeel);
		l.setPreferredSize(new Dimension(l_width, 30));
		l.setToolTipText(getKey());
		attrPanel.add(l);
		//创建右边下拉选项框
		JComboBox<String> combox=getCombox(reDo);
		combox.setPreferredSize(new Dimension(Right.right_width-l_width, 30));
		attrPanel.add(combox);
		
		bearer().add(attrPanel);
	}
	/**
	 * 销毁属性（删除）
	 */
	public void destroy() {
		if(attrPanel!=null)
			bearer().remove(attrPanel);
	}
	/**
	 * 获取承载版
	 */
	public JPanel bearer() {
		return Right.p_topRight;
	}
	/**
	 * 获取属性版
	 */
	public JPanel attrPanel() {
		if(attrPanel==null) {
			attrPanel=Right.createPanel(null, 30);
			attrPanel.setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
		}
		return attrPanel;
	}
}
