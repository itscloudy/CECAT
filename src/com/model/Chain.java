package com.model;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * 链
 * @author 柴家琪
 */
public class Chain {
	private boolean isNew;
	public static final String cohesion="connective",reference="coreference";
	private ArrayList<Node> nodes=new ArrayList<>();
	private boolean isCohesion;
	public Chain(TreeSet<Node> c,boolean isCohesion,boolean isNew) {
		this.isNew=isNew;//根据是否是在重做判断 是否是新的
		this.isCohesion=isCohesion;
		for (Node n:c){
			n.setBelongto(this);
			nodes.add(n);
		}
	}
	public Chain(TreeSet<Node> c,String isCohesion,boolean isNew) {
		this(c,isCohesion.equals(cohesion),isNew);
	}
	public ArrayList<Node> getNodes() {
		return nodes;
	}
	public boolean isCohesion() {
		return isCohesion;
	}
	public boolean isNew() {
		return isNew;
	}
	public void notNew() {
		isNew=false;
	}
}
