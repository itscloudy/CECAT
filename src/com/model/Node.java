package com.model;

import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.JOptionPane;

import com.util.Defaults;
import com.util.Util;
import com.util.XFile;
import com.view.Left;
import com.view.Main;
import com.view.Right;

/**
 * 链节点
 * @author 柴家琪
 */
public class Node {
	public static String splitChar="‖";//多字段分隔符
	public int preShowing=Right.NO;
	private static final String CN="cn",EN="en"; //省略对应字段
	
	//中英文省略单元位置
	private TreeSet<Integer> cn_ellipsis_cell=new TreeSet<>(),en_ellipsis_cell=new TreeSet<>(); 
//	private String cn,en;
//	private Node last,next;
	private Chain belongto;		//属于的链
	private NodePlatform platform;//显示平台
	private boolean cn_enter;	//是否由中文文本域发起
//	private int ellipsisType;//省略模式、省略单元
	//
//	private boolean cn_valid=false,en_valid=false;
	
	public HashMap<String, String> cn_attrs=new HashMap<>(),en_attrs=new HashMap<>();//属性地图
	private HashMap<String,String> cn_attrsource=new HashMap<>(),en_attrsource=new HashMap<>();//若有修改，存入原值，原值不为空说明修改过。
//	private int cn_loc=10000,en_loc=10000;
	private int cn_page_loc=-1,en_page_loc=-1;
	//头（关键部分）
	
	private TreeMap<Integer, String> cn_map=new TreeMap<>(),en_map=new TreeMap<>();
	
	//前两个是当前位置，后两个为原位置
	private int cn_head_loc=-1,en_head_loc=-1,c_hl_s=-1,e_hl_s=-1;
	private String cn_head="",en_head="",c_h_s="",e_h_s="";
	public int cn_head_cell=0,en_head_cell=0,c_hc_s=0,e_hc_s=0;//头部所在的单元，默认第一个单元
	
	private int page=0;
	/**
	 * 创建Node
	 * @param cn_enter	是否由中文域发起
	 * @param text		文字
	 */
	public Node(boolean cn_enter,String text,int page,int loc) {
		this.cn_enter=cn_enter;	//是否是由中文进入
		this.page=page;
		platform=new NodePlatform(this);
		if(cn_enter) 
			setCn(text,loc,true);	//设置文字及位置
		else 
			setEn(text,loc,true);
		
		setAttr(cn_enter, false);
		//创建“展示”平台
	}
	
//	public void setEllipsis(int ellipsisType,int ellipsis_cell) {
//		if(ell)
//	}
	/**
	 * 设置共同属性 及首个语言的属性（创建临时节点时调用）
	 * @param page 节点所在页数
	 * @param ellipsisType 省略类型
	 */
//	public void setCommonAttr(int page,int ellipsisType) {
//		this.page=page;
//		this.ellipsisType=ellipsisType;
//		this.ellipsis_cell=ellipsis_cell;
//		setAttr(cn_enter,false);//设置第一个语言属性
//	}
	/**
	 * 设置共同属性 及首个语言的属性（导入文件时）
	 * @param page	节点所在页数
	 * @param ellipsisString	省略对应字符串
	 */
//	public void setCommonAttr(int page,String ellipsisString,int ellipsis_cell) {
//		int ellip=Right.NO;
//		if(ellipsisString.equals(CN))
//			ellip=Right.CN;
//		else if(ellipsisString.equals(EN))
//			ellip=Right.EN;
//		setCommonAttr(page, ellip,ellipsis_cell);
//	}
	public String getCn() {
		if(cn_map.size()!=0)
		return cn_map.firstEntry().getValue();
	return "Error > Empty Cn";
	}
	/**
	 * 获取全部的字段的整合
	 * @param cn	是否获取中文
	 */
	public String getFullString(boolean cn) {
		TreeMap<Integer,String> map=cn?cn_map:en_map;
		if(map.size()==0) {
			JOptionPane.showConfirmDialog(null, "程序出错：Node.getFull(boolean cn)", "出错", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_OPTION);
			return null;
		}
		String s="";
		for(Integer i:map.keySet()) {
			s+=map.get(i)+splitChar;
		}
		return s.substring(0, s.length()-1);
	}
	/**
	 * 获取所有位置的整合
	 * @param cn	是否获取中文位置
	 */
	public String getFullLoc(boolean cn) {
		TreeMap<Integer,String> map=cn?cn_map:en_map;
		if(map.size()==0) {
			JOptionPane.showConfirmDialog(null, "程序出错：Node.getFull(boolean cn)", "出错", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_OPTION);
			return null;
		}
		String s="";
		for(Integer i:map.keySet()) {
			s+=i+splitChar;
		}
		return s.substring(0, s.length()-1);
	}
	/**
	 * 获取TreeMap
	 * @param cn	是否获取中文Map
	 */
	public TreeMap<Integer,String> getMap(boolean cn){
		return cn?cn_map:en_map;
	}
	/**
	 * 设置中文数据
	 * @param cn	字段
	 * @param loc	位置
	 * @param passAct	通过之后活动（修改视图，在导入节点时不需要）
	 */
	private void setCn(String cn,int loc,boolean passAct) {
		if(Util.valid(cn)) {
//			cn_valid=true;
//			this.cn = cn;
//			cn_loc=loc;
			
			cn_map.put(loc, cn);
			
			platform.setText(true);
			
			if(passAct)
				passed();
			return;
		}
//		cn_valid=false;
	}
	public String getEn() {
		if(en_map.size()!=0)
			return en_map.firstEntry().getValue();
		return "Error > Empty En";
	}
	/**
	 * 设置英文数据
	 * @param en	字段
	 * @param loc	位置
	 * @param passAct	通过之后活动（修改视图，在导入节点时不需要）
	 */
	private void setEn(String en,int loc,boolean passAct) {
		if(Util.valid(en)) {
//			en_valid=true;
//			this.en = en;
//			en_loc=loc;
			
			en_map.put(loc, en);
			
			platform.setText(false);
			
			if(passAct)
				passed();
			return;
		}
//		en_valid=false;
	}
	//TODO 自由添加
	public void freeSet(boolean isCn,String s,int loc) {
		if(isCn)
			cn_map.put(loc, s);
		else
			en_map.put(loc, s);
		platform.setText(isCn);
		//SUPPOSE 位置和属性都变为数组
	}
	public TreeMap<Integer, String> getCn_map() {
		return cn_map;
	}
	public TreeMap<Integer, String> getEn_map() {
		return en_map;
	}
	/**
	 * 设置中文头部（修改节点时）
	 * @param loc	位置
	 * @param head	头
	 */
	public void setCn_head(int cell,int loc,String head) {
		cn_head_cell=cell;
		cn_head_loc=loc;
		cn_head=head;
		Left.afterModifyNode(modified());
	}
	/**
	 * 设置英文头部（修改节点时）
	 * @param loc	位置
	 * @param head	头
	 */
	public void setEn_head(int cell,int loc,String head) {
		en_head_cell=cell;
		en_head_loc=loc;
		en_head=head;
		Left.afterModifyNode(modified());
	}
	
	public int getCn_head_loc() {
		return cn_head_loc;
	}
	public int getEn_head_loc() {
		return en_head_loc;
	}
	//FIXME 头部选择出错
	public String getCn_head() {
		if(cn_head_loc>=0) {
			if(cn_head_loc+cn_head.length()>getCn().length())
				Defaults.message("程序错误：node.getCn_head()", Defaults.ERROR);
			else
				return cn_head;
		}
		return "";
	}
	public String getEn_head() {
		if(en_head_loc>=0) {
			if(en_head.length()+en_head_loc>getEn().length())
				Defaults.message("程序错误：Node node.getEn_head(）", Defaults.ERROR);
			else
				return en_head;
		}
		return "";
	}
	/**
	 * 设置头（导入文件时）
	 * @param cn_head	中文头
	 * @param cn_head_loc	中文头位置
	 * @param en_head	英文头
	 * @param en_head_loc	英文头位置
	 */
	public void setHead(String cn_head,int cn_head_loc,String en_head,int en_head_loc) {
		this.cn_head=c_h_s=cn_head;
		this.cn_head_loc=c_hl_s=cn_head_loc;
		this.en_head=e_h_s=en_head;
		this.en_head_loc=e_hl_s=en_head_loc;
	}
	/**
	 * 是否通过（中英文是否均有效）
	 */
	public boolean pass() {
		return valid("both");
	}
	/**
	 * 设置另一个
	 * @param passAct	通过之后活动（修改视图，在导入节点时不需要）
	 */
	public void setAnother(String text,int loc,boolean passAct) {
		if(valid("cn"))
			setEn(text,loc,passAct);
		else 
			setCn(text,loc,passAct);
	}
	public NodePlatform getPlatform() {
		return platform;
	}
	/**
	 * 获取中文第一个字段位置
	 */
	public int getCn_loc() {
		return cn_map.firstKey();
//		return cn_loc;
	}
	/**
	 * 获取英文第一个字段位置
	 */
	public int getEn_loc() {
		return en_map.firstKey();
//		return en_loc;
	}
	@Deprecated
	public int getCn_page_loc() {
		if(cn_page_loc==-1) 
//			cn_page_loc=XFile.getCnOffsetOf(page)+cn_loc+page;
			cn_page_loc=XFile.getCnOffsetOf(page)+cn_map.firstKey()+page;
		return cn_page_loc;
	}
	@Deprecated
	public int getEn_page_loc() {
		if(en_page_loc==-1)
//			en_page_loc=XFile.getEnOffsetOf(page)+en_loc+page;
			en_page_loc=XFile.getEnOffsetOf(page)+en_map.firstKey()+page;
		return en_page_loc;
	}
	/**
	 * 获取在文中的位置
	 * @param isCn	是否是中文位置
	 * @param loc	段落位置
	 */
	public int getPage_loc(boolean isCn,int loc) {
		if(isCn)
			return cn_page_loc=XFile.getCnOffsetOf(page)+loc+page;
		return en_page_loc=XFile.getEnOffsetOf(page)+loc+page;
	}
	/**
	 * 获取中文属性值
	 */
	public String getCn_ValueofKey(String key) {
		if(cn_attrs.containsKey(key))
			return cn_attrs.get(key);
		return null;
	}
	/**
	 * 获取英文属性值
	 */
	public String getEn_ValueofKey(String key) {
		if(en_attrs.containsKey(key))
			return en_attrs.get(key);
		return null;
	}
//	public boolean isCohesion() {
//		return isCohesion;
//	}
//	public int getEllipsisType() {
//		return ellipsisType;
//	}
//	
//	public int getEllipsis_cell() {
//		return ellipsis_cell;
//	}
//	public void setEllipsis_cell(int ellipsis_cell) {
//		this.ellipsis_cell = ellipsis_cell;
//	}
	/**
	 * 获取对应的省略字段
	 */
//	public String getEllipsisString() {
//		if(ellipsisType==Right.NO)
//			return NO;
//		else if(ellipsisType==Right.CN)
//			return CN;
//		else
//			return EN;
//	}
	/**
	 * 处理1.5版本之前xml中的EllipsisType
	 * @param type	类型
	 */
	public void handleEllipsisType(String type) {
		if(type==null)
			return;
		if(type.equals(CN))
			cn_ellipsis_cell.add(0);
		else if(type.equals(EN))
			en_ellipsis_cell.add(0);
	}
	/**
	 * 获取省略的单元
	 * @param ofCn	是否获取中文省略的单元
	 */
	public String getEllipsisString(boolean ofCn) {
		TreeSet<Integer> cells=ofCn?cn_ellipsis_cell:en_ellipsis_cell;
		String s="";
		for(Integer i:cells) {
			s+=i+Node.splitChar;
		}
		if(s.equals(""))
			return null;
		return s.substring(0, s.length()-1);
	}
	/**
	 *检查指定字段是否通过
	 */
	public boolean valid(String field) {
		switch(field) {
		case "cn":return cn_map.size()!=0;
		case "en":return en_map.size()!=0;
			default:
				return cn_map.size()!=0&&en_map.size()!=0;
		}
	}
	/**
	 * 
	 * 返回是否中英文是否通过
	 * @param ofCn 是否查看中文可用性
	 */
	public boolean validOf(boolean ofCn) {
		if(ofCn) {
//			System.out.println("VALID CN");
			return cn_map.size()!=0;
		}
//		System.out.println("VALID EN");
		return en_map.size()!=0;
	}
	/**
	 * 设置属性（创建节点时）
	 * @param cn	是否是中文属性
	 * @param recover	是否恢复
	 */
	public void setAttr(boolean cn,boolean recover) {
//		System.out.println("Node.setAttr > "+cn+" "+recover);
		for(Attr a:Main.attrs) {
			if(cn)
				cn_attrs.put(a.getKey(),(String)a.getCombox().getSelectedItem());
			else
				en_attrs.put(a.getKey(),(String)a.getCombox().getSelectedItem());
			//设置完之后再恢复
			if(recover) {
				a.getCombox().setSelectedIndex(0);
			}
		}
	}
	/**
	 * 设置属性（导入文件时）
	 * @param key	键
	 * @param value 值
	 * @param cn	是否是中文属性
	 */
	public void setAttr(String key,String value,boolean cn) {
		if(cn)
			cn_attrs.put(key,value);
		else
			en_attrs.put(key, value);
	}
	/**
	 * 显示属性（在Platform中使用）
	 * @param cn	是否是中文
	 */
	public void showAttr(boolean cn) {
		Attr.locked=true;
		for(Attr a:Main.attrs) {
			if(cn)
				a.getCombox().setSelectedItem(cn_attrs.get(a.getKey()));
			else
				a.getCombox().setSelectedItem(en_attrs.get(a.getKey()));
		}
		Attr.locked=false;
	}
	/**
	 * 通过之后
	 */
	public void passed() {
		if(pass()) {
			setAttr(!cn_enter,true);//设置第二个语言属性
			Right.addNode(this);//添加节点
			Right.resetRBEnable();//重置视图
			Left.setLast_n_NextEnable(true);//重置上下段按钮的可用性
		}
	}
	/**
	 * 设置属于的链，在加入链集合时由链设置
	 * @param belongto
	 */
	public void setBelongto(Chain belongto) {
		this.belongto = belongto;
	}
	/**
	 * 修改属性
	 * @param cn	是否是中文属性
	 * @param key	键
	 * @param value	值
	 */
	public void modifyAttr(boolean cn,String key,String value) {
		if(cn) {
			if(cn_attrsource.containsKey(key)) {
			}
			if(!cn_attrsource.containsKey(key)) {			//若Source中不包含key
				cn_attrsource.put(key, cn_attrs.get(key));	//原值放入Source
//				cn_modified=true;
			}else if(cn_attrsource.get(key).equals(value)) {//如果Source中包含key，且value值和Source中的相同，从Source中移除
				cn_attrsource.remove(key);
//				cn_modified=cn_attrsource.size()!=0;		//Source是否为空决定了是否被修改过
			}
			cn_attrs.put(key, value);	//把修后的值放入
		}else {
			if(!en_attrsource.containsKey(key)) {
				en_attrsource.put(key, value);
//				en_modified=true;
			}else if(en_attrsource.get(key).equals(value)) {
				en_attrsource.remove(key);
//				en_modified=en_attrsource.size()!=0;
			}
			en_attrs.put(key, value);
		}
		Left.afterModifyNode(modified());//修改保存键可用性
	}
	/**
	 * 是否被修改
	 * 判断依据先后：属性、单元、位置、文字
	 */
	public boolean modified() {
		return cn_attrsource.size()!=0||en_attrsource.size()!=0||cn_head_cell!=c_hc_s||en_head_cell!=e_hc_s
				||cn_head_loc!=c_hl_s||en_head_loc!=e_hl_s||!cn_head.equals(c_h_s)||!en_head.equals(e_h_s);
	}
	/**
	 * 获取属于的链
	 */
	public Chain getBelongto() {
		return belongto;
	}
	
	
	public int getPage() {
		return page;
	}
	/**
	 * 判断某一单元是否是省略字段
	 * @param cn	中文字段
	 * @param cell	单元值
	 */
	public boolean ellipsisOf(boolean cn,int cell) {
//		System.out.println((cn?"CN":"EN")+" "+cell);
		if(cn)
			return cn_ellipsis_cell.contains(cell);
		return en_ellipsis_cell.contains(cell);
	}
	/**
	 * 判断省略字段是否为空
	 * @param cn	中文字段
	 */
	public boolean emptyEllipsisOf(boolean cn) {
		if(cn)
			return cn_ellipsis_cell.isEmpty();
		return en_ellipsis_cell.isEmpty();
	}
//	public TreeSet<Integer> getCn_ellipsis_cell() {
//		return cn_ellipsis_cell;
//	}
//	public TreeSet<Integer> getEn_ellipsis_cell() {
//		return en_ellipsis_cell;
//	}
	/**
	 * 获取指定字段的位置
	 * @param ofCn	查看中文字段
	 */
	public int getLoc(boolean ofCn) {
		return ofCn?getCn_loc():getEn_loc();
	}
	/**
	 * 重置展示平台可用性
	 */
	public void setPlatformEnable() {
		platform.setEnable(XFile.getPrepage()==page||!Right.holder);
	}
	/**
	 * 获取头所在位置单元
	 * @param ofCn	中文单元
	 */
	public String getHeadCellText(boolean ofCn) {
		return getCellText(ofCn,getHeadCell(ofCn));
	}

	/**
	 * 获取指定位置单元值
	 * @param ofCn	是否中文单元
	 * @param cell	单元值
	 */
	public String getCellText(boolean ofCn,int cell) {
		int index=0;
		if(ofCn) {
//			System.out.println(cn_head_cell+" CN HEAD CELL");
			for(Integer i:cn_map.keySet()) {
				if(cell==index)
					return cn_map.get(i);
				index++;
			}
		}else {
//			System.out.println(en_head_cell+" EN HEAD CELL");
			for(Integer i:en_map.keySet()) {
				if(cell==index)
					return en_map.get(i);
				index++;
			}
		}
		return "Error > Empty Map";
	}
	/**
	 * 获取单元值
	 * @param ofCn 中文单元值
	 */
	public int getHeadCell(boolean ofCn) {
		return ofCn?cn_head_cell:en_head_cell;
	}
	/**
	 * 设置单元
	 * @param ofCn	设置中文单元
	 * @param cell	单元值
	 */
	public void setHeadcell(boolean ofCn,int cell) {
		if(ofCn)
			cn_head_cell=cell;
		else
			en_head_cell=cell;
	}
	/**
	 * 设置单元（导入文件时）
	 * @param cn_head_cell 	中文单元
	 * @param en_head_cell	英文单元
	 */
	public void setHeadCell(int cn_head_cell,int en_head_cell) {
		this.cn_head_cell=c_hc_s=cn_head_cell;
		this.en_head_cell=e_hc_s=en_head_cell;
	}
	/**
	 * 设置省略（导入文件时）
	 * @param cn_ellipsis_cell	中文省略
	 * @param en_ellipsis_cell	英文省略
	 */
	public void setEllipsisCell(String cn_cell,String en_cell) {
		if(cn_cell!=null) {
			String cns[]=cn_cell.split(Node.splitChar);
			for(String s:cns) {
				cn_ellipsis_cell.add(Integer.parseInt(s));
			}
		}
		if(en_cell!=null) {
			String ens[]=en_cell.split(Node.splitChar);
			for(String s:ens) {
				en_ellipsis_cell.add(Integer.parseInt(s));
			}
		}
	}
	/**
	 * 设置省略（在创建或设置更多字段后调用)
	 * @param forCn	是否是中文省略
	 * @param cell	
	 */
	public void setEllipsisCell(boolean forCn) {
		System.out.println("Node.setEllipsisCell > "+forCn);
		//map当前的size-1就是要标记为省略的单元
		if(forCn)
			cn_ellipsis_cell.add(cn_map.size()-1);
		else
			en_ellipsis_cell.add(en_map.size()-1);
	}
	/**
	 * 改变单元
	 * @param ofCn	改变中文单元
	 * @param value	改变值为0或负数减一，否则加一
	 */
	public void changeHeadCell(boolean ofCn,int value) {
		value=value>0?1:-1;
		if(ofCn)
			cn_head_cell+=value;
		else
			en_head_cell+=value;
		//检查单元值
//		cn_head_cell=cn_head_cell<0?0:cn_head_cell>=cn_map.size()?cn_map.size()-1:cn_head_cell;
//		en_head_cell=en_head_cell<0?0:en_head_cell>=en_map.size()?en_map.size()-1:en_head_cell;
	}
}
