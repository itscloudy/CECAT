package com.model;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import com.util.Defaults;
import com.view.Main;
import com.view.NodeShower;
import com.view.Right;
/**
 * 链节点Panel，用于显示节点
 * @author 柴家琪
 *
 */
@SuppressWarnings("serial")
public class NodePlatform extends JPanel{
	private Node node;
	private JLabel cn,en;
	private boolean enable=true;
	public NodePlatform(Node node) {
		this.node=node;
		//创建标签
		cn=new NodeLabel(this, "[选择中文字段]",true);
		en=new NodeLabel(this, "[select english field]",false);
		
		//其他设置
		setPreferredSize(new Dimension((Main.width-Main.int_left-30)/5-5,60));
		setLayout(new GridLayout(2, 1));
		setBorder(new LineBorder(Color.WHITE, 1));
		
		add(cn);
		add(en);
	}
	public JLabel getCn() {
		return cn;
	}
	class NodeLabel extends JLabel{
		private NodeLabel(final NodePlatform platform,String text,final boolean isCn) {
			
			setFont(Defaults.defaultFont);
			setHorizontalAlignment(JLabel.CENTER);
			setOpaque(true);
			setText(text);
			setBackground(Color.WHITE);
			setForeground(Color.white);
			addMouseListener(new MouseAdapter() {
				Color source_bg;
				public void mouseEntered(MouseEvent e) {
					if(enable) {
						platform.setBorder(new LineBorder(Color.black, 1));
						source_bg=getBackground();
						setBackground(Defaults.border_color);
						setForeground(Color.BLACK);
					}
				}
				public void mouseExited(MouseEvent e) {
					if(enable) {
						platform.setBorder(new LineBorder(Color.WHITE, 1));
						setBackground(source_bg);
						setForeground(Color.white);
					}
				}
				public void mouseClicked(MouseEvent e) {
					if(enable&&node.pass()) {//若节点可用，且节点创建完全结束
						switch(e.getButton()) {
						case MouseEvent.BUTTON3:
							new NodeShower(node);break;
//							if(Defaults.comfirm( "确认删除节点？"))
//								Right.removeNode(node);
						default:
							Right.recoverLast();	//恢复上一显示节点
							modify(isCn);			//显示
//							Right.preShowingArea=isCn?Right.CN:Right.EN;//当前显示的文本域
							node.showAttr(isCn);
							Right.preShowing=node;
						}
					}
				}
			});
		}
	}
	/**
	 * 设置文字
	 * @param isCn	是中文字段
	 * @param text	字段
	 */
	public void setText(boolean isCn) {
		JLabel n=isCn?cn:en,other=isCn?en:cn;
		String text=node.getFullString(isCn);
		n.setText(text);
		n.setToolTipText(text);
		n.setBackground(Color.DARK_GRAY);
		if(!node.validOf(!isCn))
			other.setBackground(Color.LIGHT_GRAY);
	}

	/**
	 * 在文章中显示节点
	 * @param isCn 是否主显中文
	 */
	public void modify(boolean isCn){
		node.preShowing=isCn?Right.CN:Right.EN;
		Right.show(isCn, node, Color.RED, true);
		Right.show(!isCn, node, Color.green, true);
//		Defaults.modifyColor(Color.RED);
//		if(isCn) {
//			if(node.getEllipsisType()==Right.CN)
//			if(node.cn_ellipsis_cell.size()>0)
//				Defaults.modifyColor(Color.blue);
//			Right.show(true,node,node.getCn_loc(),Color.RED,true);
//			Defaults.modifyColor(Color.GREEN);
//			Right.show(false,node,node.getEn_loc(),Color.GREEN,true);
//			System.out.println(node.getCn()+" "+node.getCn_loc()+" cn&loc");
//		}else {
//			if(node.getEllipsisType()==Right.EN)
//			if(node.en_ellipsis_cell.size()>0)
//				Defaults.modifyColor(Color.blue);
//			Right.show(false,node,node.getEn_loc(),Color.red,true);
//			Defaults.modifyColor(Color.GREEN);
//			Right.show(true,node,node.getCn_loc(),Color.green,true);
//		}
	}
	public Node getNode() {
		return node;
	}
	public void setEnable(boolean enable) {
		this.enable=enable;
		if(!enable) {
			cn.setBackground(Color.LIGHT_GRAY);
			en.setBackground(Color.LIGHT_GRAY);
		}else {
			cn.setBackground(Color.DARK_GRAY);
			en.setBackground(Color.DARK_GRAY);
		}
	}
}
