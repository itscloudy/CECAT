package com.util;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.Comparator;
import java.util.TreeSet;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
/**
 * 静态产生默认的各种组件
 * @author 柴家琪
 *
 */
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import com.model.Node;
import com.view.Main;
import com.view.Right;

/**
 * 用于生成默认的组件，或者执行通用操作
 * @author 柴家琪
 */
public class Defaults {
	public static final Font defaultFont=new Font("微软雅黑",Font.BOLD,14);	//默认样式
	public static final Color border_color=Color.LIGHT_GRAY,defaultFeel=new Color(232,242,254),offsetFeel=new Color(205,232,255);
	public static final int ERROR=-1,WARNING=1,CONFIRM=2,SCROLL=11,NO_SCROLL=-11;		//消息类型
	/**
	 * 创建分割条
	 * @param bg		背景色
	 * @param height	高度
	 */
	public static JPanel devider(int height,Color bg) {
		JPanel p=panel(bg, null);
		p.setPreferredSize(new Dimension(height,1));
		return p;
	}
	/**
	 * 获取指定条约的滚动版
	 * @param component	组件
	 * @param vsbPolicy	纵向条约
	 * @param hsbPolicy	横向条约
	 */
	public static JScrollPane scroller(JComponent component,int vsbPolicy,int hsbPolicy) {
		switch(vsbPolicy) {
		case NO_SCROLL:vsbPolicy=JScrollPane.VERTICAL_SCROLLBAR_NEVER;break;
		default:vsbPolicy=JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED;
		}
		switch(hsbPolicy) {
		case NO_SCROLL:hsbPolicy=JScrollPane.HORIZONTAL_SCROLLBAR_NEVER;break;
		default:hsbPolicy=JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED;
		}
		JScrollPane js=new JScrollPane(component, vsbPolicy, hsbPolicy);
		js.setBorder(null);
		return js;
	}
	public static JScrollPane scroller(JComponent component) {
		return scroller(component,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	}
	/**
	 * 创建默认分割面板
	 * @param direction 方向
	 * @param loc		分割位置
	 */
	public static JSplitPane spliter(int direction,int loc) {
		JSplitPane js=new JSplitPane(direction);
		js.setDividerSize(0);
//		js.setEnabled(false);
		js.setDividerLocation(loc);
		js.setBorder(null);
		return js;
	}
	/**
	 * 设置sp的左右组件
	 */
	public static void fillSpliter(JSplitPane sp,JComponent left,JComponent right) {
		sp.setLeftComponent(left);
		sp.setRightComponent(right);
	}
	/**
	 * 创建默认面板
	 * @param bg 背景色
	 */
	public static JPanel panel(Color bg,String title) {
		JPanel panel=new JPanel();
		if(bg!=null)
			panel.setBackground(bg);
		if(title!=null)
			panel.setBorder(border(title));
		
		Dragger.drag(Main.m, panel);//在面板上可拖动窗体
		return panel;
	}
	/**
	 * 创建默认边框
	 * @param name 框名
	 */
	public static Border border(String name) {
		return new TitledBorder(new LineBorder(border_color, 1),name, TitledBorder.LEFT, TitledBorder.BELOW_TOP, new Font("微软雅黑", Font.BOLD,14), border_color);
	}
	/**
	 * 创建默认网格约束
	 */
	public static GridBagConstraints constraints() {
		GridBagConstraints gbc=new GridBagConstraints();
		gbc.fill=GridBagConstraints.BOTH;
		gbc.gridx=0;gbc.gridy=0;
		return gbc;
	}
	/**
	 * 创建默认按钮
	 * @param text 按钮文字
	 */
	public static JButton button(String text,boolean enable) {
		return button(text,null,enable);
	}
	/**
	 * 创建带图标的按钮
	 * @param text	按钮文字
	 * @param icon	图标
	 */
	public static JButton button(String text,String icon,boolean enable) {
		JButton jb=new JButton(text, icon(icon));
		jb.setFocusPainted(false);//设置被选中后虚线边框不显示
		jb.setFont(defaultFont);
		jb.setEnabled(enable);
		
		Dragger.drag(Main.m, jb);//在按钮上可拖动窗体
		return jb;
	}
	/**
	 * 显示提示框
	 * @param message	文字
	 * @param type		类型
	 */
	public static int message(String message,int type) {
		switch(type) {
		case CONFIRM:
			return JOptionPane.showConfirmDialog(null, message, "请确认",JOptionPane.YES_NO_OPTION);
		case ERROR:
			return JOptionPane.showOptionDialog(null, message, "错误", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
		case WARNING:
			return JOptionPane.showOptionDialog(null, message, "注意", JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);
		default:
			return JOptionPane.showOptionDialog(null, message, "消息", JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
		}
	}
	/**
	 * 显示确认消息
	 */
	public static boolean comfirm(String message) {
		return JOptionPane.showConfirmDialog(Main.m, message, "请确认", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)==JOptionPane.OK_OPTION;
	}
	/**
	 * 创建默认单选按钮
	 * @param text 按钮文字
	 */
	public static JRadioButton radio(String text,Color bg) {
		JRadioButton jrb=new JRadioButton(text);
		if(bg!=null)
			jrb.setBackground(bg);
		jrb.setFocusPainted(false);
		jrb.setFont(defaultFont);
		jrb.setEnabled(false);
		return jrb;
	}
	/**
	 * 创建默认标签
	 * @param text 标签文字
	 */
	public static JLabel label(String text,Color bg,Color fg) {
		JLabel jl=new JLabel(text);
		jl.setFont(defaultFont);
		jl.setHorizontalAlignment(JLabel.CENTER);
		if(bg!=null) {
			jl.setOpaque(true);
			jl.setBackground(bg);
		}
		if(fg!=null)
			jl.setForeground(fg);
		return jl;
	}
	/**
	 * 创建默认输入框
	 * @param bg	背景色
	 */
	public static JTextField field(String text,Color bg,Color border) {
		final JTextField f=new JTextField();
		f.setText(text);
		f.setFont(defaultFont);
		f.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		if(bg!=null)
			f.setBackground(bg);
		if(border==null)
			f.setBorder(null);
		else
			f.setBorder(new LineBorder(border, 1));
		return f;
	}
//	/**
//	 * 设置文字域是否可用
//	 * @param area
//	 * @param enable
//	 */
//	public static void setAreaEnable(JTextArea area,boolean enable) {
//		if(enable)
//			area.setBackground(Color.WHITE);
//		else
//			area.setBackground(null);
//	}
//	public static void setAreaEnable(JTextPane area,boolean enable) {
//		if(enable)
//			area.setBackground(Color.WHITE);
//		else
//			area.setBackground(null);
//	}
	/**
	 * 设置输入框是可用
	 * @param field		输入框
	 * @param enable	可用性
	 */
	public static void setFieldEnable(JTextField field,boolean enable) {
		if(enable) {
			field.setBackground(Color.white);
			field.setBorder(new LineBorder(Color.DARK_GRAY, 1));
		}else {
			field.setBackground(Color.LIGHT_GRAY);
			field.setBorder(null);
		}
		field.setEnabled(enable);
	}
	/**
	 * 创建默认复选框
	 * @param list	复选列表
	 */
	public static JComboBox<String> combox(String[] list){
		JComboBox<String> c;//TIP 或许有机会看看为何无法设置背景色
		if(list!=null)
			c=new JComboBox<String>(list);
		else
			c=new JComboBox<String>();
		c.setFocusable(false);
		c.setFont(defaultFont);
		return c;
	}
	/**
	 * 获取TreeSet
	 * @param byCn	以byCn排序
	 */
	public static TreeSet<Node> treeset(boolean byCn){
		TreeSet<Node> set;
		if(byCn)
			set=new TreeSet<>(new Comparator<Node>() {
				public int compare(Node n0, Node n1) {
					if(n0.getPage()!=n1.getPage())
						return n0.getPage()>n1.getPage()?1:-1;
					return n0.getCn_loc()>n1.getCn_loc()?1:n0.getCn_loc()<n1.getCn_loc()?-1:n0.getCn().equals(n1.getCn())?0:1;
				}
			});
		else
			set=new TreeSet<>(new Comparator<Node>() {
				public int compare(Node n0, Node n1) {
					if(n0.getPage()!=n1.getPage())
						return n0.getPage()>n1.getPage()?1:-1;
					return n0.getEn_loc()>n1.getEn_loc()?1:n0.getEn_loc()<n1.getEn_loc()?-1:n0.getEn().equals(n1.getEn())?0:1;
				}
			});
		return set;
	}
	public static ImageIcon icon(String name) {
		if(name==null)
			return null;
		return new ImageIcon(Defaults.class.getResource(name+".png"));
	}
	/**
	 * 根据name获取Image对象
	 */
	public static Image iconImage(String name) {
		return Toolkit.getDefaultToolkit().getImage(Defaults.class.getResource(name+".png"));
	}
	/**
	 * 获取属性集
	 */
	public static  SimpleAttributeSet getAttrSet() {
		SimpleAttributeSet attr=new SimpleAttributeSet();
		StyleConstants.setFontFamily(attr, "微软雅黑");
		return attr;
	}
	/**
	 * 修改颜色属性
	 */
	public static void modifyColor(Color modifyTo){
		StyleConstants.setForeground(Right.attr_set, modifyTo);
	}
}
