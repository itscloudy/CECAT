package com.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

import javax.swing.JComponent;

import com.enter.Start;
import com.model.Attr;
import com.view.Main;

public class Util {
	/**
	 * 求两个数中最小值
	 */
	public static int min(int a, int b) {
		return a>b?b:a;
	}
	/**
	 * 求两个数中的最大值
	 */
	public static int max(int a,int b) {
		return a>b?a:b;
	}
	/**
	 * 判断字符串是否有效
	 */
	public static boolean valid(String str) {
		return str!=null&&!(str.equals(""));
	}
	
	//一下为配置文件操作方法
	
	/**
	 * 读取配置文件
	 */
	public static void readProperties() {
		Proper proper = getProper();
        Enumeration<Object> enumer = proper.keys();
		while(enumer.hasMoreElements()){
			String key = (String)enumer.nextElement();
			String value = proper.getProperty(key);//获取配置
			System.out.println("Read :"+key+" _ "+value);
			String[] candidates = value.split(Main.dot);
			Main.attrs.add(new Attr(key, candidates));
		}
	}
	/**
	 * 获取Proper对象
	 */
	public static Proper getProper() {
		Proper proper = new Proper();
		try {
			File file = new File(Start.parent_path+Main.config);
			if(!file.exists()){
				file.createNewFile();
			}
			InputStream inputStream = new FileInputStream(file);
			proper.load(inputStream);
		} catch (IOException e1) {
			Defaults.message(e1.getMessage()+"原因："+e1.getCause()+"。", Defaults.ERROR);
		}
		return proper;
	}
	/**
	 * 储存更改
	 */
	public static void storeProper(Proper proper) {
		try {
//			FileOutputStream s=new FileOutputStream(file)
			FileOutputStream steam = new FileOutputStream(Start.parent_path+Main.config);
			proper.store(steam, "Attributes");
			steam.close();
		} catch (IOException e) {
			e.printStackTrace();
			Defaults.message(e.getMessage()+"原因："+e.getCause()+"。", Defaults.ERROR);
		}
	}
	/**
	 * 刷新组件
	 */
	public static void refreshComponent(JComponent c) {
		c.setVisible(false);
		c.setVisible(true);
	}
}
