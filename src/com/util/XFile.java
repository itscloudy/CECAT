package com.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import com.model.Chain;
import com.model.Node;
import com.view.Left;
import com.view.Main;
import com.view.Right;

import pri.file.FOperator;

/**
 * 文档对象
 * @author fuzhenyu | 柴家琪
 */
public class XFile {
	private static File file,parentFile;
	private static String source="",context="",filePath;
	//当前读取的段的索引
	private static int prepage=0;
	//文档的内容
//	private TreeMap<Integer, Integer> pgs=new TreeMap<>();
	private static String[] pgs,cn_pgs,en_pgs;
	private static int[] cn_offsets,en_offsets;
	private static boolean emptyFile=false;
	
	/**
	 * 切换文件
	 * @param file
	 */
	public static boolean turnToFile(File file) {
		if(Left.chains!=null&&!Left.saved()) {
			if(Defaults.comfirm("是否保存链？")) {
				output();
				Right.clearChain();//改变文档后清空链
			}
		}
		
		regetSource(file);
		
		if(source.length()<=0) {
			Defaults.message("空文件！_turn read", Defaults.WARNING);
			return false;
		}
		
		XFile.file=file;
		return reset();//重置
	}
	public static boolean readFile(){
		if(!readDoc())//若没有读取文档返回的
			return false;
		
		regetSource(file);
		return reset();
	}
	/**
	 * 重置
	 */
	public static boolean reset() {
		if(source.length()<=0) {
			emptyFile=true;
			Defaults.message("空文件！_first read",Defaults.WARNING);
			return false;//空文件返回
		}else
			emptyFile=false;
		
		filePath=file.getAbsolutePath();//获取路径
		parentFile=file.getParentFile();//获取父元素
		
		context="";
		
		char chars[]=source.toCharArray();
		for(int i=0;i<chars.length;i++) {
			if(chars[i]==10||chars[i]=='\0'||chars[i]==13){
				chars[i]=10;//error point:doc中的换行是13，文本域对于字符13是不会自动换行的，要换成10
				i++;
				if(i==source.length()){//如果是最后一个字符，保存为最后一段（最后一段不存在，仅仅为了指示倒数第二段的截止位置）
					break;
				}
				boolean stop=false;
				int num_of_10=1;
				while(chars[i]==10||chars[i]=='\0'||chars[i]==13){
					chars[i]=10;
					i++;
					num_of_10++;
					if(i==source.length()){//同上
						stop=true;
						break;
					}
				}
				if(stop||chars[i]==3||chars[i]==4)//3是本文结束符，4是传输结束符
					break;
				if(num_of_10>=1)		//如果换行符多于一个
					context+="○"+chars[i];//添加段落分隔符号，方便后面的分段
				else
					context+=chars[i];
			}else
				context+=chars[i];
		}
		source=new String(chars);	//重新赋值source（置换13为10）
		pgs=context.split("○");		//添加段落
		///////////////
//		for(int i=source.length()-40;i<source.length();i++) {
//			System.out.print(source.charAt(i)+""+(int)source.charAt(i)+" .");
//		}
//		System.out.println();
//		System.out.println(source.substring(source.length()-40));
		
		if(pgs.length<2) {
			Defaults.message("只读取到一段或者未读取到，请检查文档", Defaults.WARNING);
			return false;//只有一段返回
		}
		if(pgs.length%2!=0) {
			if(Defaults.comfirm("<html>非偶数段落数，是否抛弃最后一段？<br/>（最后一段：\""+pgs[pgs.length-1]+"\")</html>")) {
				String ps[]=new String[pgs.length-1];
				for(int i=0;i<ps.length;i++) {
					ps[i]=pgs[i];
				}
				pgs=ps;
			}else
				return false;//非偶数段返回
		}
		
		
		context=context.replaceAll("○", "");//重新赋值context（去除分隔符○）
		cn_pgs=new String[pgs.length/2];
		en_pgs=new String[pgs.length/2];
		cn_offsets=new int[pgs.length/2];
		en_offsets=new int[pgs.length/2];
		int cn_offset=0,en_offset=0;
		//分拨中英文段落
		for(int i=0;i<pgs.length;i++) {
			if(i%2==0) {
				cn_pgs[i/2]=pgs[i];
				cn_offsets[i/2]=cn_offset;
				cn_offset+=pgs[i].length();
			}
			else {
				en_pgs[i/2]=pgs[i];
				en_offsets[i/2]=en_offset;
				en_offset+=pgs[i].length();
			}
		}
		prepage=0;//当前段落设置为第一段
		
		//重置Left中的组件
		input();
		Left.resetFile(file);
		return true;
	}
	public static String getTextExcepted13(String text){
		String toReturn="";
		for(int i=0;i<text.length();i++){
			if(text.charAt(i)!=13)
				toReturn+=text.charAt(i);
		}
		return toReturn;
	}
	/**
	 * 如果文件不为空
	 */
	public static boolean fileNotNull(){
		return file!=null;
	}
	public static File getFile(){
		return file;
	}
	public static String getFilePath(){
		return filePath;
	}
	public static File getParent() {
		return parentFile;
	}
	public static boolean isEmptyFile() {
		return emptyFile;
	}
	/**
	 * 获取中文段
	 */
	public static String[] getCn_pgs(){
		return cn_pgs;
	}
	/**
	 * 获取英文段
	 */
	public static String[] getEn_pgs(){
		return en_pgs;
	}
	public static int getCnOffsetOf(int page) {
		return cn_offsets[page];
	}
	public static int getEnOffsetOf(int page) {
		return en_offsets[page];
	}
	/**
	 * 获取中文某段的长度
	 */
	public static int getCnLenOfPg(int pg) {
		return cn_pgs[pg].length();
	}
	/**
	 * 获取英文某段的长度
	 */
	public static int getEnLenOfPg(int pg) {
		return en_pgs[pg].length();
	}
	/**
	 * 获取首段
	 */
	public static ArrayList<String> getFirstPra(){
		prepage=0;
		ArrayList<String> arr = new ArrayList<String>();
		if(prepage<cn_pgs.length) {
			arr.add(cn_pgs[prepage]);
			arr.add(en_pgs[prepage]);
		}
//		if(currentParaIndex<paragraphs.length-1)
//		arr.add(paragraphs[++currentParaIndex]);
//		if(currentParaIndex<paragraphs.length-1)
//		arr.add(paragraphs[++currentParaIndex]);
		return arr;
	}
	
	/**
	 * 获取上一段
	 */
	public static  ArrayList<String> getLastPara(){
		ArrayList<String> arr = new ArrayList<String>();
		if(prepage==0){
			return arr;
		}
		prepage--;
		arr.add(cn_pgs[prepage]);
		arr.add(en_pgs[prepage]);
		return arr;
	}
	/**
	 * 获取下一段
	 */
	public static ArrayList<String> getNextPara(){
		ArrayList<String> arr = new ArrayList<String>();
		if(prepage==cn_pgs.length-1){
			return arr;
		}
		prepage++;
		arr.add(cn_pgs[prepage]);
		arr.add(en_pgs[prepage]);
		return arr;
	}
	public static boolean hasLastPara() {
		return prepage!=0;
	}
	public static boolean hasNextPare() {
		return prepage!=cn_pgs.length-1;
	}
	public static int getPrepage() {
		return prepage;
	}
	public static boolean isLastestPage() {
		return prepage==cn_pgs.length-1;
	}
	private static boolean readDoc(){
		file=findFile("docx","doc");
		if(file==null) 
			return false;
		return true;
	}
	/**
	 * 重置source值
	 */
	private static void regetSource(File file) {
		if(FOperator.compare(file, "doc"))
			source=s_of_Doc(file);
		else
			source=s_of_Docx(file);
	}
	/**
	 * 添加新的Docx文件
	 * @author 柴家琪
	 */
	private static String s_of_Docx(File file){
		String str = "";
        try {
            FileInputStream fis = new FileInputStream(file);
            XWPFDocument xdoc = new XWPFDocument(fis);
            XWPFWordExtractor extractor = new XWPFWordExtractor(xdoc);
            str = extractor.getText();
            extractor.close();
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
	}
	/**
	 * 添加新的Doc文件
	 * @author 柴家琪
	 */
	private static String s_of_Doc(File file){
		String str = "";
        try {
            FileInputStream fis = new FileInputStream(file);
            HWPFDocument doc = new HWPFDocument(fis);
            str = doc.getDocumentText();
            doc.close();
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
	}
	/**
	 * 获取指定的文件
	 * @param kind 后缀
	 * @author 柴家琪
	 */
	public static File findFile(final String ...kind){
		JFileChooser jfc=new JFileChooser();
		//清除所有文件选项
		jfc.removeChoosableFileFilter(jfc.getFileFilter());
		jfc.addChoosableFileFilter(new FileFilter() {
			public String getDescription() {
				String des="";
				for(int i=0;i<kind.length;i++){
					des+=kind[i];
					if(i+1<kind.length)
						des+="、";
				}
				return des+"文件";
			}
			public boolean accept(File f) {
				if(f.isDirectory())
					return true;
				for(int i=0;i<kind.length;i++){
					if(f.getName().endsWith(kind[i]))
						return true;
				}
				return false; 
			}
		});
		jfc.setDialogTitle("请选择文件");
		jfc.setDialogType(JFileChooser.SAVE_DIALOG);//Mac可能不支持Open_Dialog
		if(jfc.showDialog(null, "选择/打开")==1)
			return null;
		return jfc.getSelectedFile();
	}
	/**
	 * 导出到XML
	 */
	public static void output() {
		File out;
		Left.checkPreChain();
		
		if(Left.chains.size()!=0&&Defaults.comfirm("导出到源目录？")) {
			Defaults.message(getParent()+"\\"+getFile().getName().split("\\.")[0]+".xml", Defaults.ERROR);
			out=new File(getParent()+"\\"+getFile().getName().split("\\.")[0]+".xml");
		}
		else {
			JFileChooser jfc=new JFileChooser();
			jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			jfc.setDialogTitle("选择导出XML目录");
			jfc.showOpenDialog(Main.m);
			File selected=jfc.getSelectedFile();
			if(selected!=null)
				out=new File(selected.getPath()+"\\"+getFile().getName().split("\\.")[0]+".xml");
			return;
		}
		//写入
		Element root=new Element("Chains");
		root.setAttribute("file", getFile().getName());
		for(Chain c:Left.chains) {
			root.addContent(chainElement(c));
		}
		
//		Document d=new Document(root);
		//填入Format参数可以在TXT等编辑器下自动换行显示
		XMLOutputter XMLOut = new XMLOutputter(Format.getPrettyFormat());
	    try{
	    	XMLOut.output(new Document(root), new FileOutputStream(out));
	    } catch (FileNotFoundException e) {
	    	Defaults.message(e.getMessage(), Defaults.ERROR);
	    	e.printStackTrace();
	    } catch (IOException e){
	    	Defaults.message(e.getMessage(), Defaults.ERROR);
	    	e.printStackTrace();
	    } 
	}
	/**
	 * 获取段落的element
	 */
	private static Element chainElement(Chain c) {
		Element root=new Element("chain");
		root.setAttribute("mapping",c.isCohesion()?Chain.cohesion:Chain.reference);
		root.setAttribute("type", "A");
		for(Node n:c.getNodes()) {
			Element e=new Element("node");
			//中文属性
			e.setAttribute("cn", n.getFullString(true));
			e.setAttribute("cn_loc", n.getFullLoc(true)+"");
			//中文头部
			e.setAttribute("cn_head",n.getCn_head());
			e.setAttribute("cn_head_loc",n.getCn_head_loc()+"");
				//中文头部单元
			if(n.cn_head_cell!=0)
				e.setAttribute("cn_head_cell",n.cn_head_cell+"");//头部单元为0，不设置
			//中文省略
			String cs=n.getEllipsisString(true);
			if(cs!=null)
				e.setAttribute("cn_ellipsis_cell", cs);
			
			//英文属性
			e.setAttribute("en", n.getFullString(false));
			e.setAttribute("en_loc",n.getFullLoc(false)+"");
			//英文头部
			e.setAttribute("en_head",n.getEn_head());
			e.setAttribute("en_head_loc",n.getEn_head_loc()+"");
				//英文头部单元
			if(n.en_head_cell!=0)
				e.setAttribute("en_head_cell",n.en_head_cell+"");
			//英文省略
			String es=n.getEllipsisString(false);
			if(es!=null)
				e.setAttribute("en_ellipsis_cell", es);
			
			//所在段落
			e.setAttribute("pagegraph",n.getPage()+"");
			//添加属性节点
			Element cn_attr=new Element("cn_attr");
			for(String key:n.cn_attrs.keySet()) {
				cn_attr.setAttribute(key, n.cn_attrs.get(key));
			}
			Element en_attr=new Element("en_attr");
			for(String key:n.en_attrs.keySet()) {
				en_attr.setAttribute(key, n.en_attrs.get(key));
			}
			e.addContent(cn_attr);
			e.addContent(en_attr);
			root.addContent(e);
		}
		return root;
	}
	/**
	 * 导入XML
	 */
	private static void input() {
		File xml=new File(parentFile.getAbsolutePath()+"/"+file.getName().split("\\.")[0]+".xml");
		if(xml.exists()) {
			try {
		    	SAXBuilder builder = new SAXBuilder();
		    	Document doc = builder.build(xml); 
		    	Element root = doc.getRootElement();
		    	if(file.getName().equals(root.getAttributeValue("file"))) {//查看文件是否对应
		    		//创建链集合
		    		ArrayList<Chain> chains=null;
		    		for(Element c:root.getChildren()) {
		    			if(chains==null)
		    				chains=new ArrayList<>();
		    			//创建节点排序树
		    			TreeSet<Node> set=null;
		    			for(Element n:c.getChildren()) {
		    				if(set==null)
		    					set=Defaults.treeset(true);
		    				//设置基本信息
		    				String cn=n.getAttributeValue("cn"),cn_loc=n.getAttributeValue("cn_loc"),cn_head=n.getAttributeValue("cn_head"),
		    						cn_head_loc=n.getAttributeValue("cn_head_loc"),cn_head_cell=n.getAttributeValue("cn_head_cell"),
		    						cn_ellipsis_cell=n.getAttributeValue("cn_ellipsis_cell"),
		    						en=n.getAttributeValue("en"),en_loc=n.getAttributeValue("en_loc"),en_head=n.getAttributeValue("en_head"),
		    						en_head_loc=n.getAttributeValue("en_head_loc"),en_head_cell=n.getAttributeValue("en_head_cell"),
		    						page=n.getAttributeValue("pagegraph"),ellip=n.getAttributeValue("ellipsis"),
		    						en_ellipsis_cell=n.getAttributeValue("en_ellipsis_cell");
		    				
		    				//遍历导入节点字段
		    				Node node = null;
		    				String[] cns=split(cn),ens=split(en),cn_locs=split(cn_loc),en_locs=split(en_loc);
//		    				System.out.println(cn+" "+en);
		    				for(int i=0;i<cns.length;i++) {
//		    					System.out.println("cn_l : "+cns[i]+" "+cn_locs[i]);
		    					if(node==null)
		    						node=new Node(true,cns[i],inte(page),inte(cn_locs[i]));
		    					else
		    						node.freeSet(true, cns[i], inte(cn_locs[i]));
		    				}
		    				for(int i=0;i<ens.length;i++) {
//		    					System.out.println("en_l : "+ens[i]+" "+en_locs[i]);
		    					node.freeSet(false, ens[i], inte(en_locs[i]));
		    				}
		    				
		    				//处理1.5版本之前的ellispsis属性
		    				node.handleEllipsisType(ellip);
		    				
		    				//设置省略
		    				node.setEllipsisCell(cn_ellipsis_cell, en_ellipsis_cell);
		    				//导入所在段落和省略策略的属性
//		    				Node node=new Node(true, cn, inte(cn_loc));
//		    				node.setCommonAttr(inte(page), ellip,inte_cell(ellip_cell));
//		    				node.setAnother(en, inte(en_loc),false);
		    				if(cn_head!=null&&en_head!=null)
		    					node.setHead(cn_head, inte(cn_head_loc), en_head, inte(en_head_loc));
		    				//导入属性
		    				Element cn_attr=n.getChild("cn_attr"),en_attr=n.getChild("en_attr");
		    				for(Attribute attr:cn_attr.getAttributes()) {
		    					node.setAttr(attr.getName(), attr.getValue(), true);
		    				}
		    				for(Attribute attr:en_attr.getAttributes()) {
		    					node.setAttr(attr.getName(), attr.getValue(), false);
		    				}
		    				//导入头单元值
		    				node.setHeadCell(inte_cell(cn_head_cell), inte_cell(en_head_cell));
		    				set.add(node);//添加节点
		    			}
		    			if(set!=null)//！理论上不可能
		    				chains.add(new Chain(set,c.getAttributeValue("mapping"),false));//添加链
		    		}
		    		if(chains!=null) 
		    			Left.chains=chains;
		    		Left.refillTree(false);
		    		return;
		    	}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
		if(Left.chains!=null) {
			Left.chains.clear();
			Left.refillTree(false);
		}
	}
	/**
	 * 使数字字符串转变为数字
	 */
	private static int inte(String s) {
		return Integer.parseInt(s);
	}
	/**
	 * 使数字字符串转变为数字,如果传入空值，返回0
	 */
	private static int inte_cell(String s) {
		if(s==null)
			return 0;
		return Integer.parseInt(s);
	}
	private static String[] split(String s) {
		return s.split(Node.splitChar);
	}
}
