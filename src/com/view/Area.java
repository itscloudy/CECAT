package com.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;

import com.util.Defaults;

/**
 * 文本域
 * @author 柴家琪
 */
@SuppressWarnings("serial")
public class Area extends JTextPane{
	private Color bg;//背景色
	private StyledDocument doc;
	public final boolean forCn;	//是否显示中文
	/**
	 * @param forCn	是否是显示中文
	 * @param bg	背景色
	 */
	public Area(boolean forCn,Color bg) {
		this.forCn=forCn;
		if(bg!=null)
			this.bg=bg;
		else
			this.bg=Color.white;

		setBackground(this.bg);
		setBorder(null);
		
		int size=14;
		switch(Main.s_i_z_e()) {
		case Main.BIG_SIZE:size=20;break;
		case Main.MIDDLE_SIZE:size=16;break;
			default:
		}
		setFont(new Font("微软雅黑",Font.BOLD,size));//大号
		setEditable(false);
		setSelectedTextColor(Color.white);
		setSelectionColor(Color.DARK_GRAY);
//		setSelectionStart(Area.ALLBITS);

		if(forCn)
			setBorder(Defaults.border("中文"));
		else
			setBorder(Defaults.border("英文"));
		doc=this.getStyledDocument();
	
	}
	public void setEnabled(boolean enabled) {
//		super.setEnabled(enabled);
		setBackground(enabled?bg:null);
	}
	/**
	 * 根据select，改变光标
	 * @param select 是否在选择省略位置
	 */
	public void selectEllipLoc(boolean select){
		if (select)
			setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		else
			setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}
	public StyledDocument getDoc() {
		return doc;
	}
	public void paint(Graphics g) {
		super.paint(g);
		int i=192,d=0;
		while(i>=70) {
			int x=192-i+d;
			if(i%50==0)
				d+=3;
			g.setColor(new Color(318-i,318-i,318-i));
			g.drawLine(x, 0, x+3, 2);
			g.drawLine(0, x, 2, x+3);
			i--;
		}
		 
//		int i=0,s=192;
//		while(i<=384) {
//			int x=s<0?-s:s;
//			g.setColor(new Color(x,x,x));
//			g.drawLine(i, 0, i+3, 2);
//			i++;
//			s--;
//		}
	}
}
