package com.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

import com.model.Attr;
import com.util.Defaults;
//import com.util.Proper;
import com.util.Proper;
import com.util.Util;

/**
 * 属性设置窗口<br/>
 * @author 柴家琪
 */
@SuppressWarnings("serial")
public class AttrSetter extends JDialog{
	private final int width=1000,height=618,topheight=552;
	//上外面板（放置上内面板，上内面板大小随组价增多而增多，外面板提供滚动）、上内面板、下面板
	private JPanel top_outter,top,bottom;
	private JButton add,delete,up,down,save;
	private ArrayList<Item> items=new ArrayList<>();
	
	private final int original_items_num;	//原始数量
	private Item preItem=null,lastItem=null,newest=null;//当前组件
	private boolean size_changed=false;		//上部分大小是否改变
	private int index=0;		//原始链次序
	
	/**
	 * 内部类：承载板（载具，主要用于交换视图组件）
	 * @author 柴家琪
	 */
	class Bearer extends JPanel{
		private Item driver=null;
		public Bearer(Item driver) {
			setPreferredSize(new Dimension(width, 30));
			setLayout(new GridLayout(1, 1));

			exchangeDriver(driver);
		}
		/**
		 * 移除乘客再添加新乘客
		 */
		public void exchangeDriver(Item driver) {
			if(this.driver!=null)
				remove(this.driver);
			
			this.driver=driver;
			driver.setBearer(this);
			add(driver);
		}
	}
	/**
	 * 内部类：属性组件
	 * @author 柴家琪
	 *
	 */
	class Item extends JPanel{
		private Attr attr;
		private Bearer bearer;	
		private final String oKey,oValue;	//原始字段
		private final boolean isNew;		//是否是新添加
		private final int link_index;		//原始链次序
		private JSplitPane sp;
		private JTextField f_key,f_value;
		public boolean key_changed=false,value_changed=false;;
		private Cursor out_cursor=new Cursor(Cursor.DEFAULT_CURSOR),in_cursor=new Cursor(Cursor.TEXT_CURSOR);
		private Item thisone,last,next;
		public Item(Attr attr,boolean isNew){
			this.attr=attr;
			oKey=attr.getKey();
			oValue=attr.getValueString();
			thisone=this;	
			this.isNew=isNew;
			link_index=index++;		//保存时比较大小次序，若有乱序执行删除重做，否则执行移除添加
			
			sp=Defaults.spliter(JSplitPane.HORIZONTAL_SPLIT, (int)(width*0.2));
			sp.setDividerSize(5);
			sp.setEnabled(true);
			
			f_key=Defaults.field(oKey,new Color(245,245,245),null);
			f_value=Defaults.field(oValue,new Color(245,245,245),null);
			Defaults.fillSpliter(sp, f_key, f_value);
			
			inputerListen(f_key,f_value,true);
			inputerListen(f_value,f_key,false);
			
			setLayout(new GridLayout(1, 1));
			add(sp);
		}
		private void inputerListen(final JTextField field,final JTextField another,final boolean isKey) {
			field.addKeyListener(new KeyAdapter() {
				public void keyTyped(KeyEvent e) {
					Thread t=new Thread(new Runnable() {
						public void run() {
							try {
								Thread.sleep(100);//因为，Type监听在输入法输入之前，为保证获取到修改后的文字此处延时
								judgeField(isKey, field);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							
						}
					});t.start();
				
				}
			});
			field.addFocusListener(new FocusListener() {
				//先失去焦点再获得
				public void focusLost(FocusEvent e) {
					field.setCursor(out_cursor);
					another.setBackground(new Color(245,245,245));
				}
				public void focusGained(FocusEvent e) {

					preItem=thisone;
					
					field.selectAll();
					field.setCursor(in_cursor);
					another.setBackground(Defaults.offsetFeel);
					
					setOperatorEnable();
					
				}
			});
		}
		/**
		 * 判断文本框中文字是否和原始相同，不同的save按钮可用
		 */
		private void judgeField(boolean isKey,JTextField field) {
			String s=field.getText();

			if(isKey) {
				if(!oKey.equals(s)) {
					key_changed=true;
					save.setEnabled(true);
				}else {
					key_changed=false;
					setSaverEnable();
				}
			}else {
				if(!oValue.equals(s)) {
					value_changed=true;
					save.setEnabled(true);
				}else {
					value_changed=false;
					setSaverEnable();
				}
			}
		}
		public String key() {
			return f_key.getText();
		}
		public String value() {
			return f_value.getText();
		}
		public int getIndex() {
			return link_index;
		}
		public boolean hasLast() {
			return last!=null;
		}
		public Item getLast() {
			return last;
		}
		public void setLast(Item last) {
			this.last=last;
		}
		public boolean hasNext() {
			return next!=null;
		}
		public Item getNext() {
			return next;
		}
		public void setNext(Item next) {
			if(next!=null)
				next.last=this;
			this.next = next;
		}
		public Bearer getBearer() {
			return bearer;
		}
		public void setBearer(Bearer bearer) {
			this.bearer = bearer;
		}
		
	}
	
	/**
	 * 属性设置窗口
	 */
	protected AttrSetter() {
		top_outter=new JPanel();top=new JPanel();bottom=new JPanel();
		top_outter.setPreferredSize(new Dimension(width-5,topheight));
		top_outter.setLayout(new GridLayout(1, 1));
		top_outter.add(Defaults.scroller(top));
		
		top.setBackground(Color.white);
		top.setPreferredSize(new Dimension(width-5,topheight));
		top.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 1));
		
		//添加原始属性
		Iterator<Attr> ia=Main.attrs.iterator();
		while(ia.hasNext()) {
			addItem(new Item(ia.next(),false), false);
		}
		original_items_num=items.size();//设置原始组件数量
		resetView();
		if(items.size()>0)
			items.get(0).f_value.requestFocus();
		
		//*****************************************************
		bottom.setPreferredSize(new Dimension(width-6,height-topheight-30));
		bottom.setLayout(new GridLayout(1, 5));
		add=addButton("添加","attr_add",true);
		delete=addButton("删除","attr_delete",false);
		up=addButton("上移","up",false);
		down=addButton("下移","down",false);
		save=addButton("保存","attr_save",false);
		
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(keyIllegal())
					return;
				Attr newAttr=new Attr("","无");
				Item newItem=new Item(newAttr,true);
				addItem(newItem,true);			//组件集合、视图添加
				newItem.f_key.requestFocus();
				newest=newItem;		//最新添加组件
				
				save.setEnabled(true);
			}
		});
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(preItem!=null) {
					if(preItem.getNext()!=null)
						preItem.getNext().f_value.requestFocus();//如果有下一个，下一个获取焦点
					
					if(preItem.hasLast())
						preItem.getLast().setNext(preItem.getNext());//从链中删除
					
					if(preItem==newest)
						newest=null;
					
					preItem.attr.destroy();			//在主视图中销毁（关键点：如果最后不保存，主视图不会刷新，此项依旧存在。保存过后会消失）
					items.remove(preItem);			//从组件集合中移除
					top.remove(preItem.getBearer());//从top面板中移除

					setSaverEnable();	//设置save按钮可用性
					resetView();  		//重置视图
				}
			}
		});
		up.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(preItem!=null&&preItem.hasLast()) {
					Item last=preItem.getLast();
					if(last.hasLast())
						last.getLast().setNext(preItem);
					else
						preItem.setLast(null);//画图可知，若pre无上一元素，要将将要置换的下一个的last设为null
					last.setNext(preItem.getNext());
					preItem.setNext(last);
					
					swap(preItem,last);
				}
			}
		});
		down.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(preItem!=null&&preItem.hasNext()) {
					Item next=preItem.getNext();
					if(preItem.hasLast())
						preItem.getLast().setNext(next);
					else
						next.setLast(null);	//画图可知，若pre无上一元素，要将将要置换的下一个的last设为null
					preItem.setNext(next.getNext());
					next.setNext(preItem);
					
					swap(preItem,next);
				}
			}
		});
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(existSameKey()||keyIllegal())
					return;
				
				Main.attrs.clear();
				//重置属性面板视图 以及 改动properties前参数准备
				int redo_index=-1,last_link_index=-1,pre_index=-1;	//重做位置、上一链位置、当前位置（enumer位置）
				for(Item pre:items) {	//第一遍循环检测下一步要进行的操作，面板可以同时添加
					Main.attrs.add(pre.attr);//重新添加属性
					if(pre.key_changed||pre.value_changed||pre.isNew) {
						pre.attr.setKey(pre.key());	//重置key
						pre.attr.setValueString(pre.value());	//重置values
						pre.attr.addToBearer(true);	//重新加载combox
					}
					else
						pre.attr.addToBearer(false);//不加载combox
					if(redo_index<0) {	//之后从redo_index=link_index 开始执行删除，否则执行移除,最后添加
						if(pre.link_index<last_link_index||pre.key_changed||pre.isNew)
							redo_index=pre.link_index;
						else
							last_link_index=pre.link_index;
					}
				}
				
				Proper proper=Util.getProper();
				Set<String> keys=proper.stringPropertyNames();
				
				//开始对properits进行操作
				Iterator<Item> ii=items.iterator();
				Item item = null;//实际上不可能出现null的情况，但程序表象上是有可能的
				last_link_index=-1;//上一链位置
				for(String key:keys) {
					pre_index++;
					if(pre_index<redo_index||redo_index<0) {//需要删除且在标记点前或不需要删除
						if(pre_index>last_link_index) {		//当前位置
							if(ii.hasNext()) {
								item=ii.next();
								last_link_index=item.link_index;
							}else {
								proper.remove(key);
								continue;
							}
						}
						if(pre_index<last_link_index) {
							proper.remove(key);
						}else {
							if(item.value_changed) {
								proper.setProperty(key, item.value());
							}
						}
					}else	//需要删除且在标记点后，执行删除
						proper.remove(key);
				}
				if(item!=null&&item.isNew)
					proper.setProperty(item.key(),item.value());
				while(ii.hasNext()) {//修改删除（移除）完之后，item还有，则开始添加
					item=ii.next();
					proper.setProperty(item.key(),item.value());
				}
				
				Util.storeProper(proper);
				Util.refreshComponent(Right.p_topRight);
				
				Right.b_attrSet.setEnabled(true);
				Right.setAttrEnable();//设置属性们的可用性
				setVisible(false);
			}
		});
		//以下为默认设置和添加等**********************************
		setModal(true);//设置窗口闪烁（阻止焦点丢失）
		setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
		add(top_outter);add(bottom);
		setTitle(Main.TITLE+" - 属性控制面板");
		setBounds((Main.screenSize.width-width)/2, (Main.screenSize.height-height)/2, width, height);
		setResizable(false);
		setVisible(true);
	}
	/**
	 * 添加新组件
	 */
	private void addItem(Item item,boolean resetView) {
		if(lastItem==null)
			lastItem=item;
		else {
			while(lastItem.hasNext())
				lastItem=lastItem.getNext();
			lastItem.setNext(item);
			lastItem=item;	//添加完一个设置当前组件为最新添加的组件
		}
		
//		top.add(item);		//视图添加
		top.add(new Bearer(item));
		items.add(item);	//集合添加
		if(resetView)
			resetView();
	}
	
	/**
	 * 重置视图（增减组件数量，并根据数量改变宽高）
	 */
	private void resetView() {
		int left=items.size()-17;
		
		if(left>0&&!size_changed) {
			top.setPreferredSize(new Dimension(width-22,topheight+left*30));
			size_changed=true;
		}else if(size_changed){
			top.setPreferredSize(new Dimension(width-5,topheight));
			size_changed=false;
		}
		Util.refreshComponent(top);
	}
	/**
	 * 新增按钮
	 */
	private JButton addButton(String text,String icon,boolean enable) {
		JButton button=Defaults.button(text, icon,enable);
		bottom.add(button);
		button.setFocusable(false);
		return button;
	}
	/**
	 * 设置除Add按钮外的3个操作按钮的可用性
	 */
	private void setOperatorEnable() {
		if(preItem!=null) {
			delete.setEnabled(true);
			up.setEnabled(preItem.getLast()!=null);
			down.setEnabled(preItem.getNext()!=null);
		}else {
			delete.setEnabled(false);
			up.setEnabled(false);
			down.setEnabled(false);
		}
	}
	/**
	 * 设置save按钮的可用性
	 */
	private void setSaverEnable() {
		if(original_items_num!=items.size()) {
			save.setEnabled(true);
			return;
		}else {
			Item last=null;
			for(Item i:items) {
				//有新的属性、键或值被修改、排序改变
				if(i.isNew||i.key_changed||i.value_changed) {
					save.setEnabled(true);
					return;
				}
				if(last!=null&&i.link_index<last.link_index) {
					save.setEnabled(true);
					return;
				}
				last=i;
			}
		}
		save.setEnabled(false);
	}
	/**
	 * 检查是否存在相同的key
	 */
	private boolean existSameKey() {
		ArrayList<String> keys=new ArrayList<>();
		for(Item i:items) {
			if(keys.contains(i.key())) {
				Defaults.message("存在相同的键值："+i.key(), Main.ERROR);
				return true;
			}
			else
				keys.add(i.key());
		}
		return false;
	}
	/**
	 * 检查是否存在空键
	 */
	private boolean keyIllegal() {
		if(newest!=null) {
			String illegal_message="";
			
			//属性格式限制
			String key=newest.f_key.getText();
			
			
			if(!newest.key_changed||key.length()<1)
				illegal_message="请设置上一新加属性的键值";
			else{
				char first=key.charAt(0);
				
				if((first>='0'&&first<='9')||first=='_') 
					illegal_message="名称不能以数字和 _（下划线）开头\n";
				if(key.toLowerCase().startsWith("xml")) 
					illegal_message+="名称不能以 xml 开头\n";
				if(key.contains(" ")||key.contains(":")) 
					illegal_message+="名称不能包含空格和 ：（冒号）\n";
			}
			
			if(!illegal_message.equals("")) {
				newest.f_key.requestFocus();
				Defaults.message(illegal_message, Defaults.WARNING);
				return true;
			}
		}
		return false;
	}
	/**
	 * 交换集合以及视图位置
	 */
	private void swap(Item a,Item b) {
		Collections.swap(items, items.indexOf(a), items.indexOf(b));
		
		Bearer temp=b.getBearer();
		a.getBearer().exchangeDriver(b);
		temp.exchangeDriver(a);
		resetView();
		
		preItem.f_value.requestFocus();
		setSaverEnable();
	}
}
