package com.view;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import com.model.Attr;
import com.model.Chain;
import com.model.Node;
import com.util.Defaults;
import com.util.Util;
import com.util.XFile;

/**
 * @author 柴家琪
 */
@SuppressWarnings("serial")
public class ChainShower extends JDialog{
//	public static boolean redoing=false,source_mapping=false;//是否在重做
	
	private final int width=1200,height=742;
	public static final Color sign=new Color(200,200,200);//标记颜色
	
	private JSplitPane bossSpliter,topSpliter,leftSpliter,rightSpliter;
	public Area cnArea,enArea;
	public JPanel nodePanel,attrPanel,operatePanel;

	private Chain chain;
	private Area tempCn,tempEn;//临时存放主视图的文本域，结束时返还
	
	private JButton b_redo,b_destroy,b_last,b_next;
	
	protected ChainShower(final Node prenode){
		this.chain=prenode.getBelongto();//获取属于的链
		//文本域
		cnArea=new Area(true,Color.white);
		cnArea.setText(XFile.getCn_pgs()[0]);
		enArea=new Area(false,Color.white);
		enArea.setText(XFile.getEn_pgs()[0]);
		//添加窗口监听
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {//进入时
				Right.recoverLast();
				Right.preShowing=prenode;//还原主窗体中的显示的节点
				
				prenode.getPlatform().modify(true);
			}
			@Override
			public void windowClosing(WindowEvent e) {//退出时
				whileClosing();
			}
		});
			//链显示板
		nodePanel=Defaults.panel(null, "链");
		nodePanel.setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
		//切换文本域和显示颜色
		tempCn=Right.ta_chinese;//暂寄指针
		tempEn=Right.ta_english;
		Right.ta_chinese=cnArea;//暂借指针
		Right.ta_english=enArea;
		
		Right.holder=false;
		resetChain();
			//属性显示板
			//FIXME 修改属性修改面板
		attrPanel=Defaults.panel(Defaults.offsetFeel, "属性");
		attrPanel.setLayout(new FlowLayout(FlowLayout.LEFT,0,1));
		for(Attr a:Main.attrs) {
//			a.getCombox().setSelectedIndex(0);
			
			if(prenode.cn_attrs.containsKey(a.getKey()))
				a.getCombox().setSelectedItem(prenode.cn_attrs.get(a.getKey()));
			attrPanel.add(a.attrPanel());//从主视图转移属性显示/设置组件
		}
		//按钮
		b_redo=Defaults.button("重做","redo", true);
		b_redo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(Defaults.comfirm("确定重做？")) {
					Left.checkPreChain();
					Right.preChain.addAll(chain.getNodes());
					Right.refillChain();
					Right.resetP_ChainHeight();
					Right.resetButtonEnable();
					if(chain.isCohesion())
						Right.rb_cohesion.setSelected(true);
					else
						Right.rb_reference.setSelected(true);
					
					Left.refreshFile(false);
					Left.deleteChain(chain);
					
					dispose();
				}
			}
		});
		b_destroy=Defaults.button("删除","delete", true);
		b_destroy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(Defaults.comfirm("删除此链？")) {
					int index=Left.chains.indexOf(chain);
					Left.deleteChain(chain);
					Left.refreshFile(false);
					Right.preShowing=null;
					if(Left.chains.size()<=0)
						dispose();
					else
						setChain(index==0?0:index-1);
				}
			}
		});
		b_last=Defaults.button("上一条链", Left.chains.indexOf(chain)!=0);
		b_last.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index=Left.chains.indexOf(chain);
				setChain(--index);
			}
		});
		b_next=Defaults.button("下一条链", Left.chains.indexOf(chain)!=Left.chains.size()-1);
		b_next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index=Left.chains.indexOf(chain);
				setChain(++index);
			}
		});
			//按钮板
		operatePanel=Defaults.panel(Color.white, "操作");
		JPanel top_opearte=new JPanel(),bottom_opearte=new JPanel();
		operatePanel.setLayout(new GridLayout(2, 1));
		operatePanel.add(top_opearte);operatePanel.add(bottom_opearte);
		top_opearte.setLayout(new GridLayout(2, 1));bottom_opearte.setLayout(new GridLayout(1, 2));
		top_opearte.add(b_redo);top_opearte.add(b_destroy);
		bottom_opearte.add(b_last);bottom_opearte.add(b_next);
		//分割面板布局************
		topSpliter=Defaults.spliter(JSplitPane.VERTICAL_SPLIT,300);
		Defaults.fillSpliter(topSpliter,  Defaults.scroller(cnArea), Defaults.scroller(enArea));
		leftSpliter=Defaults.spliter(JSplitPane.VERTICAL_SPLIT,600);
		Defaults.fillSpliter(leftSpliter, topSpliter, Defaults.scroller(nodePanel, Defaults.NO_SCROLL, Defaults.SCROLL));
		rightSpliter=Defaults.spliter(JSplitPane.VERTICAL_SPLIT, 500);
		Defaults.fillSpliter(rightSpliter, attrPanel, operatePanel);
		bossSpliter=Defaults.spliter(JSplitPane.HORIZONTAL_SPLIT,width-Right.right_width-15);
		Defaults.fillSpliter(bossSpliter, leftSpliter, rightSpliter);
		add(bossSpliter);
		//***************窗体设置***************
		setLayout(new GridLayout(1, 1));
		setModal(true);//设置窗口闪烁（阻止焦点丢失）
		setTitle(Left.chains.indexOf(chain));
		setBounds((Main.screenSize.width-width)/2, (Main.screenSize.height-height)/2, width, height);
		setResizable(false);
		setVisible(true);
	}
	private void setChain(int index) {
		chain=Left.chains.get(index);
		resetChain();
		b_last.setEnabled(index!=0);
		b_next.setEnabled(index!=Left.chains.size()-1);
		setTitle(index);
	}
	/**
	 * 重置链
	 */
	private void resetChain() {
		if(chain!=null) {
			Left.refreshFile(false);
			//添加节点
			nodePanel.removeAll();
			for(Node node:chain.getNodes()){
				nodePanel.add(node.getPlatform());
				node.setPlatformEnable();
				Right.recoverByScence(node);
			}
			Util.refreshComponent(nodePanel);
		}
	}
	private void setTitle(int index) {
		setTitle(Main.TITLE+" - "+ XFile.getFilePath()+" 第"+(index+1)+"条链");
	}
	/**
	 * dispose
	 */
	public void dispose() {
		whileClosing();
		setVisible(false);
//		super.dispose();
	}
	/**
	 * 窗口关闭操作
	 */
	private void whileClosing() {
		Right.ta_chinese=tempCn;
		Right.ta_english=tempEn;//返还指针
		Right.holder=true;		//返还主持权
		
		Left.refreshFile(false);
		
		Right.preShowing=null;	//复原显示方式
		
		for(Attr a:Main.attrs) {//属性重置为默认
			a.getCombox().setSelectedIndex(0);
			a.addToBearer(false);
		}
		Util.refreshComponent(Right.p_topRight);
	}
}
