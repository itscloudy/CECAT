package com.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.model.Chain;
import com.model.Node;
import com.model.TreeNode;
import com.util.Defaults;
import com.util.Dragger;
import com.util.XFile;

import pri.file.FOperator;

/**
 * 左页面
 * @author fuzhenyu | 柴家琪
 */
@SuppressWarnings("serial")
public class Left extends JPanel{
	public static boolean combox_lock=false;
	private static boolean saved=true;
	public static int source_size_of_chains=0;
	//分隔符上部分百分比
	private final double d_percent=0.2,d_percent_top=0.82;
	private static int d_boss,d_top;
	//要读取的文档
	public static File file;
	public static File[] fils= {};
	//分割面板
	private JSplitPane sp;
	
	//上面板
	private static JTree tree;					//树
	private static DefaultMutableTreeNode root;//根节点
	//下面板
	private JSplitPane sp_top;
	private JPanel p_top_l;
	private JPanel p_top_r;
	private static JPanel p_tree;
	private static JButton b_readDoc,b_save,b_lastPara, b_nextPara;
	
	//文件夹下的所有文件的下拉选项框
	private static JComboBox<String> cb_files;

	/**
	 * 所有链
	 */
	public static ArrayList<Chain> chains=new ArrayList<>();
	/**
	 * 左页面传入右页面的引用
	 * @param rightpanel 右页面对象
	 */
	

	/**
	 * 面板的构造方法
	 */
	public Left(Main m){
		d_boss=(int)(m.getHeight()*d_percent);//上面板高度
		d_top=(int)(Main.int_left*d_percent_top);//上面板分隔符位置
		initComp();
	}

	/**
	 * 初始化左页面上的组件
	 */
	private void initComp(){
		this.setLayout(new GridLayout(1,1));
		
		//上面板
		p_tree=Defaults.panel(null,"关系链");
		p_tree.setLayout(new GridLayout(1,1));
		//创建树
		root=new DefaultMutableTreeNode("R");
		DefaultTreeModel model=new DefaultTreeModel(root);//默认树模型
		tree=new JTree(model);
		tree.setRootVisible(false);
		tree.setBackground(p_tree.getBackground());
		
		Dragger.drag(Main.m, tree);//使在树上可以拖动窗体
		Renderer renderer=new Renderer();//渲染器
		renderer.setBackground(p_tree.getBackground());
		tree.setCellRenderer(renderer);
		tree.setShowsRootHandles(true);
		
		tree.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getButton()==MouseEvent.BUTTON3&&tree.getLastSelectedPathComponent()!=null) {
					new ChainShower(((TreeNode) tree.getLastSelectedPathComponent()).getNode());
				}
			}
		});
		p_tree.add(tree);
		//添加节点测试
		DefaultMutableTreeNode n1=new DefaultMutableTreeNode("");
		root.add(n1);

		tree.expandPath(new TreePath(root.getPath()));
		//上左版
		p_top_l=Defaults.panel(Color.white, null);
//		p_top_l.setLayout(new FlowLayout(FlowLayout.LEFT,0,6));
		p_top_l.setLayout(new GridLayout(3, 1,3,3));
			//按钮
		b_readDoc = createButton("读取文档","folder",true);
		b_readDoc.setEnabled(true);
		b_readDoc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(XFile.readFile())
					refreshFile(true);
			}
		});
			//保存键
		b_save=createButton("保存","save",false);
		b_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				XFile.output();
				XFile.reset();
				
				source_size_of_chains=chains.size();
				resetSaveEnable();
//				resetSaveEnable();
			}
		});
		//上右版
		p_top_r=Defaults.panel(Color.WHITE, null);
		p_top_r.setLayout(new GridLayout(2, 1));
			//上下段落按钮
		b_lastPara=getFontedButton("<html><font color='lightblue'>上<br/>段</font></html>");
//		b_lastPara = Defaults.button("<html><font color='red'>上<br/>段</font></html>",false);
//		b_lastPara.setMargin(new Insets(1,1,1,1));
		b_lastPara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(file==null) {//！理论上不可能
					Defaults.message("文档空！_Left", Defaults.ERROR);
					return;
				}
				
				recover_before_reinsert();
				ArrayList<String> arr = XFile.getLastPara();
				if(arr.size() == 2){
					Right.setParaChinese(arr.get(0));
					Right.setParaEnglish(arr.get(1));
				}
				b_lastPara.setEnabled(XFile.hasLastPara());
				b_nextPara.setEnabled(XFile.hasNextPare());
				
				resetNodeEnable();
				Main.setPageTitle(XFile.isLastestPage());
			}
		});
		b_nextPara=getFontedButton("<html><font color='lightblue'>下<br/>段</font></html>");
//		b_nextPara =Defaults.button("<html>下<br/>段</html>",false);
//		b_nextPara.setFont(new Font("微软雅黑", Font.BOLD, 8));
//		b_nextPara.setMargin(new Insets(1,1,1,1));
		b_nextPara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(file==null) {//！理论上不可能
					Defaults.message("文档空！_Left", Defaults.ERROR);
					return;
				}
				
				recover_before_reinsert();
				ArrayList<String> arr = XFile.getNextPara();
				if(arr.size() == 2){
					Right.setParaChinese(arr.get(0));
					Right.setParaEnglish(arr.get(1));
				}
				b_lastPara.setEnabled(XFile.hasLastPara());
				b_nextPara.setEnabled(XFile.hasNextPare());
				
				resetNodeEnable();
				Main.setPageTitle(XFile.isLastestPage());
			}
		});
		setFilesCombox();
			//上左版添加
		p_top_l.add(b_readDoc);
		p_top_l.add(cb_files);
		p_top_l.add(b_save);
		p_top_r.add(b_lastPara);
		p_top_r.add(b_nextPara);
		//上分割版
		sp_top=Defaults.spliter(JSplitPane.HORIZONTAL_SPLIT, d_top);
		sp_top.setBackground(p_top_l.getBackground());
		sp_top.setBorder(Defaults.border("文件操作"));
		sp_top.setPreferredSize(new Dimension(Main.int_left,d_boss));
		
		sp_top.setLeftComponent(p_top_l);
		sp_top.setRightComponent(p_top_r);
		
		sp=Defaults.spliter(JSplitPane.VERTICAL_SPLIT, d_boss);
		sp.setLeftComponent(sp_top);
		sp.setRightComponent(Defaults.scroller(p_tree));
		
		add(sp);
	}
	/**
	 * 重置文件
	 */
	public static void resetFile(File file) {
		Left.file=file;
		Left.fils=FOperator.getFiles(file.getParentFile());
		Left.setFilesCombox();
		combox_lock=false;
		saved=true;
	}
	/**
	 * 重新刷新文件或插入文字<br/>
	 * 如果是新文件，重新设置原始链数。如不是新文件，仅仅刷新文字。
	 */
	public static void refreshFile(boolean newFile) {
		if(XFile.fileNotNull()) {//读取的文件不为空
			Right.opened();
			opened(true);
			Defaults.modifyColor(Right.recovery);
			if(newFile) {
				source_size_of_chains=chains.size();
				resetSaveEnable();
			}
			recover_before_reinsert();
			if(Right.holder) {
				ArrayList<String> arr = XFile.getFirstPra();
				Main.setPageTitle(XFile.isLastestPage());
				if(arr.size() == 2){
					Right.setParaChinese("");
					Right.setParaEnglish("");
					Right.setParaChinese(arr.get(0));
					Right.setParaEnglish(arr.get(1));
				}else
					Defaults.message("文档读取错误 Left refreshFile", Defaults.ERROR);
			}else {
				String cn="",en="";
				for(String s:XFile.getCn_pgs()) {
					cn+=s+(char)10;
				}
				for(String s:XFile.getEn_pgs()) {
					en+=s+(char)10;
				}
				Right.setParaChinese(cn);
				Right.setParaEnglish(en);
			}
		}	
	}
	/**
	 * 创建复选框
	 */
	public static void setFilesCombox(){
		//防止在设置复选框时触发复选框监听
		combox_lock=true;
		if(cb_files==null) {
			cb_files=Defaults.combox(null);
//			cb_files.setPreferredSize(new Dimension(d_top-10,d_boss/3-10));
			cb_files.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					//error point:单选和复选的监听有选中被取消之分
					if(e.getStateChange()==ItemEvent.SELECTED&&!cb_files.getSelectedItem().equals(file.getName())) {
						if(!Left.combox_lock) {
							if(XFile.turnToFile(new File(XFile.getParent()+"/"+cb_files.getSelectedItem())))
								refreshFile(true);
							else {
								cb_files.setSelectedItem(file.getName());
							}
						}
					}
				}
			});
		}
		
		for(File f:fils) {
			if(FOperator.compare(f, "doc")||FOperator.compare(f, "docx")) {
//				System.out.println(f.getName());
				cb_files.addItem(f.getName());
			}
		}
		if(file!=null)
			cb_files.setSelectedItem(file.getName());
	}
	/**
	 * 创建按钮
	 * @param text 按钮文字
	 */
	private JButton createButton(String text,String icon,boolean enable) {
		JButton b=Defaults.button(text,icon,enable);
//		b.setPreferredSize(new Dimension(d_top-10,d_boss/3-10));
		return b;
	}
	private static void opened(boolean open) {
		b_lastPara.setEnabled(open&&XFile.hasLastPara());
		b_nextPara.setEnabled(open&&XFile.hasNextPare());
		p_tree.setBackground(Defaults.defaultFeel);
		tree.setBackground(p_tree.getBackground());
	}
	/**
	 * 从Right面板中添加链
	 */
	public static void addChain() {
//		System.out.println("redoing :"+ChainShower.redoing);
		Chain c=new Chain(Right.preChain,Right.isCohesion,true);
//		if(ChainShower.redoing)//若是重做状态，就不是新的链
//			c.notNew();
		chains.add(c);
		
//		saved=false;
		//重置视图
		refillTree(true);
		resetSaveEnable();//error point:不可合并在refillTree中，重置save按钮不一定都在刷新树之后
		
//		ChainShower.redoing=false;
	}
	/**
	 * 删除链
	 * @param chain 要删除的链
	 */
	public static void deleteChain(Chain chain) {
		chains.remove(chain);
		refillTree(false);
		resetSaveEnable();
	}
	/**
	 * 重新布置树
	 * @param clearPre 是否清空当前树
	 */
	public static void refillTree(boolean clearPre){
		if(clearPre)
			Right.clearChain();//清空当前树
		root.removeAllChildren();
		for (Chain c:chains){
			boolean first=true;
			DefaultMutableTreeNode node = null;
			for (Node n:c.getNodes()) {
				if (first) {
					node=new TreeNode(n);
					first = false;
				} else
					node.add(new TreeNode(n));
			}
			if(node!=null)
				root.add(node);
		}
		
		tree.updateUI();//刷新树视图
		tree.setBackground(p_tree.getBackground());
	}
	/**
	 * 重置保存键的可用性
	 */
	public static void resetSaveEnable() {
		saved=true;
		if(chains.size()!=source_size_of_chains) { //链数不同时
			saved=false;
		}
		else
			for(Chain c:chains) {
				if(c.isNew()) {
					saved=false;	//是新链时
					break;
				}
				for(Node n:c.getNodes()) {	
					if(n.modified()) {	//节点有修改时
						saved=false;
						break;
					}
				}
			}
		b_save.setEnabled(!saved);
	}
	/**
	 * 在修改属性之后，修改saved值
	 * @param 修改的节点是被修改了
	 */
	public static void afterModifyNode(boolean modified) {
		if(modified) {
			saved=false;
			b_save.setEnabled(true);
			return;
		}
		resetSaveEnable();
	}
	/**
	 * 重置节点可用性
	 * TODO 可能要在链展示板中区别对待
	 */
	private static void resetNodeEnable() {
		for(Node n:Right.preChain) {
			n.setPlatformEnable();
		}
	}
	/**
	 * 设置上下端按钮的可用性
	 * @param enable 可用性
	 */
	public static void setLast_n_NextEnable(boolean enable){
		b_lastPara.setEnabled(enable&&XFile.hasLastPara());
		b_nextPara.setEnabled(enable&&XFile.hasNextPare());
	}
	/**
	 * 当重新插入文字时恢复颜色
	 */
	private static void recover_before_reinsert() {
		Defaults.modifyColor(Right.recovery);
		Right.preShowing=null;
	}
	public static boolean saved() {
		return saved;
	}
//	public static void forceToSaved() {
//		saved=true;
//	}
	/**
	 * 检查当前链
	 */
	public static void checkPreChain() {
		if(Right.preChain!=null&&Right.preChain.size()!=0&&Defaults.comfirm( "是否保存当前链？"))
			Left.addChain();
//		ChainShower.redoing=false;
	}
	//获取上下段的按钮
	private JButton getFontedButton(String text) {
		JButton b=Defaults.button(text, false);
		int font_size;
		switch(Main.s_i_z_e()) {
		case Main.BIG_SIZE:
			font_size=14;break;
		case Main.MIDDLE_SIZE:
			font_size=12;break;
		case Main.SMALL_SIZE:
			default:
				font_size=10;
		}
		
		b.setFont(new Font("微软雅黑",Font.BOLD,font_size));
		b.setMargin(new Insets(1, 1, 1, 1));
		return b;
	}
}
/**
 * 树渲染类
 * @author 柴家琪
 */
@SuppressWarnings("serial")
class Renderer extends DefaultTreeCellRenderer{
	 public Component getTreeCellRendererComponent(JTree tree,Object value,boolean sel,  
	            boolean expanded,boolean leaf,int row,boolean hasFocus) { 
		 super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
		 
		 DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
		 if(node.isLeaf())
			 setLeafIcon(Defaults.icon("graycircle"));
		 else
			 setIcon(Defaults.icon("whitedot"));

		 setFont(Defaults.defaultFont);
		 setBackgroundNonSelectionColor(null);
		 setBackgroundSelectionColor(Color.black);
		 
		 setTextNonSelectionColor(Color.DARK_GRAY);
		 setTextSelectionColor(Color.white);
		 
		 setBorderSelectionColor(Color.white);
		 return this;
	 }
}
