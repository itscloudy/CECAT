package com.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;

import com.model.Attr;
import com.util.Defaults;
import com.util.Util;
import com.util.XFile;
/**
 * 主窗体
 * @author 柴家琪
 */
@SuppressWarnings("serial")
public class Main extends JFrame {
	public static final String TITLE="汉英指代标注平台",config="attr.properties",dot="，";
	public static final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	public static final int BIG_SIZE=100,MIDDLE_SIZE=0,SMALL_SIZE=-100;
	
	private static int SIZE;
	
	public static boolean opend=false;	//已打开文件、已保存链
//	public static int page=1;
	//左版占比
	public final double left_percent=0.18;
	public static int int_left;
	
//	private Reader reader;
	//窗体的位置及坐标
	private int x = 0;
	private int y = 0;
	public static int width,height;
	//属性
	public static ArrayList<Attr> attrs=new ArrayList<>();
	
	private Left left;
	private Right right;
	
	public static Main m;
	public void init(){
		Util.readProperties(); 	//读取配置文件
		setSystemView();	//设置系统视图
		int_left=(int)(width*left_percent);//左边宽度
        //窗体设置
        this.setTitle(TITLE);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(x, y, width, height);
		setIconImage(Defaults.iconImage("tool"));
		//左面版、右面版
		left = new Left(this);
		right = new Right(this);
		
		//分割面板
		JSplitPane spliter=Defaults.spliter(JSplitPane.HORIZONTAL_SPLIT,int_left);
        Defaults.fillSpliter(spliter, left, right);
        
        setLayout(new GridLayout(1,1));
        add(spliter);
        //窗体监听
        addWindowListener(new WindowAdapter() {
        	public void windowClosing(WindowEvent e) {
        		if(!Left.saved()&&Defaults.comfirm("还未保存，是否保存？")) {
        			XFile.output();
        		}
			}
		});
	}
	public Main(){
		Main.m=this;
		int screenWidth=screenSize.width;
		if(screenWidth>1500) {
			width=1500;height=900;
			SIZE=BIG_SIZE;
		}
		else if(screenWidth>1200) {
			width=1200;height=720;
			SIZE=MIDDLE_SIZE;
		}else if(screenWidth>1000) {
			width=1000;height=600;
			SIZE=SMALL_SIZE;
		}else 
			Defaults.message("显示设备分辨率过小！", Defaults.ERROR);
		
		x=(screenWidth-width)/2;
		y=(screenSize.height-height)/2;
		init();
		this.setVisible(true);
	}

	/**
	 * 设置系统默认视图
	 */
	public void setSystemView() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
        UIDefaults uidefs = UIManager.getLookAndFeelDefaults();
        uidefs.put("SplitPane.background", new ColorUIResource(Color.LIGHT_GRAY));//设置分隔符颜色
	}
	public static void setPageTitle(boolean last) {
		m.setTitle(TITLE+" - "+XFile.getFilePath()+" 第 "+(XFile.getPrepage()+1)+" 段"+(last?"（末段）":""));
	}
	public static int s_i_z_e() {
		return SIZE;
	}
}
