package com.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import com.model.Node;
import com.util.Defaults;
import com.util.Util;


/**
 * 节点显示框
 * @author 柴家琪
 */
@SuppressWarnings("serial")
public class NodeShower extends JDialog{
	private final int width=500,height=309;
	private Node node;
	private Area cn,en;
	private JPanel fields=new JPanel();
	private int cn_cell,en_cell;
	public NodeShower(final Node node) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		this.node=node;
		setLayout(new GridLayout(1, 1));
		
		//文本面板
		fields.setLayout(new GridLayout(2, 1));
		//添加文本域面板
		addareaPanel(true);
		addareaPanel(false);
		addFieldListener(cn);addFieldListener(en);
		//按钮面板
		JButton delete=Defaults.button("删除此节点",true);
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(Defaults.comfirm("确认删除此节点？")) {
					if(node==Right.terminal)
						Right.terminal=null;
					Right.removeNode(node);
					dispose();
				}
			}
		});
		//分割面板
		JSplitPane spliter=Defaults.spliter(JSplitPane.VERTICAL_SPLIT, 230);
		Defaults.fillSpliter(spliter, fields, delete);
		add(spliter);
		
		showHead(cn,true);
		showHead(en,true);
		//********基本设置*********
//		setLayout(new GridLayout(1, 1));
		setModal(true);//设置窗口闪烁（阻止焦点丢失）
		setTitle(Main.TITLE+" - 节点关键部分设置 | 删除节点");
		setBounds((Main.screenSize.width-width)/2, (Main.screenSize.height-height)/2, width, height);
		setResizable(false);
		setVisible(true);
	}
	/**
	 * 添加文本域的监听
	 * @param area	文本域
	 * @param isCn	是否是中文域
	 */
	private void addFieldListener(final Area area) {
		area.addMouseListener(new MouseAdapter() {
			int press;
			public void mouseReleased(MouseEvent e) {
				int release=area.getCaretPosition();
				if(press==release)//error point:应该先考虑松开和按下的位置是否相同，相同则不考虑，避免双击自动选择文本的不可抗力
					return;
				
				String select=area.getSelectedText();
				if(select!=null&&select.length()>0) {
					showHead(area,false);
					
					int cell=cell(area.forCn);
					if(area.forCn)
						node.setCn_head(cell,Util.min(press, release), select);
					else
						node.setEn_head(cell,Util.min(press, release), select);
					
					
//					resetButtonEnable(true);
					showHead(area,true);
				}
			}
			public void mousePressed(MouseEvent e) {
				press=area.getCaretPosition();
			}
		});
	}
	/**
	 * 改变头字段颜色
	 * @param area	文本域
	 * @param show	true显示false恢复
	 */
	private void showHead(Area area,boolean show) {
		Defaults.modifyColor(Color.BLACK);
		if(cell(area.forCn)!=node.getHeadCell(area.forCn))
			return;
		
		if(show)
			Defaults.modifyColor(new Color(255,200,50));
		if(area.forCn&&node.getCn_head_loc()>=0)
			Right.modify(area, node.getCn_head_loc(), node.getCn_head().length());
		else if(node.getEn_head_loc()>=0)
			Right.modify(area, node.getEn_head_loc(), node.getEn_head().length());
	}
	/**
	 * 根据字段数量添加不同的文本面板
	 * @param forCn	是否是中文面板
	 */
	private void addareaPanel(final boolean forCn) {
		final Area area=new Area(forCn, Defaults.defaultFeel);
		area.setFont(Defaults.defaultFont);
		area.setEditable(false);
		area.setText(node.getHeadCellText(forCn));
	
		if(forCn) 
			cn=area;
		else 
			en=area;
		//如果只有一个字段，不显示选择按钮
		if(node.getMap(forCn).size()==1) {
			fields.add(Defaults.scroller(area));
			return;
		}
		
		//如果有多个字段，显示选择按钮
		JPanel right=Defaults.panel(null, null);
		right.setLayout(new GridLayout(2, 1));
		
		//上下按钮
		final JButton last=Defaults.button("↑", false),next=Defaults.button("↓", true);
		right.add(last);right.add(next);
		last.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cell(area,-1,last,next);
			}
		});
		next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cell(area,1,last,next);
			}
		});
		
		JSplitPane s=Defaults.spliter(JSplitPane.HORIZONTAL_SPLIT, 450);
		Defaults.fillSpliter(s, Defaults.scroller(area), right);
		fields.add(s);
	}
	/**
	 * 获取当前显示单元
	 * @param ofCn	是否中文显示单元
	 */
	private int cell(boolean ofCn) {
		return ofCn?cn_cell:en_cell;
	}
	/**
	 * 设置当前显示单元
	 * @param area	文本域
	 * @param value	改变值
	 * @param last	向上按钮
	 * @param next	向下按钮
	 */
	private void cell(Area area,int value,JButton last,JButton next) {
		showHead(area,false);
		
		boolean ofCn=area.forCn;
		if(ofCn)
			cn_cell+=value;
		else
			en_cell+=value;
		area.setText(node.getCellText(ofCn,cell(ofCn)));
		next.setEnabled(cell(ofCn)<node.getMap(ofCn).size()-1);
		last.setEnabled(cell(ofCn)>0);
		
		showHead(area,true);
	}
}
