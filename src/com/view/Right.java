package com.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyledDocument;

import com.model.Attr;
import com.model.Node;
import com.util.Defaults;
import com.util.Util;
import com.util.XFile;
/**
 * 右页面
 * @author fuzhenyu | 柴家琪
 * 
 */
@SuppressWarnings("serial")
public class Right extends JPanel{
	private static boolean opened=false;
	public static final int NO=0,CN=1,EN=2;
	public static SimpleAttributeSet attr_set=Defaults.getAttrSet();
	public static final Color recovery=Color.black;//恢复的颜色
	//分割面板百分比 右上、右上左、右下上
	private final double d_percent_boss=0.7,d_percent_top=0.8,d_percent_bottom=0.6;
	//分割面板整数位置，载入时自动计算  ？double参数方法不可用
	private int d_int_boss,d_int_top,d_int_bottom;//分割面板位置，通过上面的百分比和主视图的宽高确定
	public static int right_width;//右边属性版宽度
	
	//当前链和当前节点
	public static TreeSet<Node> preChain=Defaults.treeset(true);

	public static Node terminal,preShowing;//末端节点、当前显示节点
	public static int preShowingArea=NO;
	//分割面板
	private JSplitPane sp_boss,sp_top,sp_bottom;//每个分割面板
	
	/*
	 * 链显示板和主面板中的文本域使用近乎相同的字段显示方法
	 * 但链显示板显示是整片显示，主面板中是分段显示
	 * 所以使用holder来使部分方法辨明“扶持”对象
	 */
	public static boolean isCohesion=true,holder=true;//是否是连接、当前是否是Right主持
	public static int ellipsis=NO;
	private JTextField tf_ellipsis;//省略输入框
	public static Area ta_chinese, ta_english;
	public static JPanel p_topRight;
	private JPanel p_topLeft;
	private JLabel l_refer,l_ellipsis;
	private JPanel p_group1,p_group2;
	private ButtonGroup group1,group2;
	public static JRadioButton rb_reference,rb_cohesion,rb_none,rb_ellipsis_cn,rb_ellipsis_en;//每个单选按钮
	public static JButton b_chooseModel,b_attrSet;//选择模式，AttrSetter控制是否可用
	private int attr_sum=0;
	private static boolean one_to_one=true;
	//下部分
	public static boolean sortByCn=true;//以中文位置顺序排序
	private static JPanel p_bottom,p_preChain;
	private static JScrollPane sc_preChain;
	private final static int chain_baseheight=120;
	private static int  chain_preheight=120;

	private JPanel p_linkOperator;
	private static JButton b_confirm,b_revoke, b_clear,b_sort;
	public Right(final Main m){
		//右边版都采用分割面板的方式布局
		d_int_boss=(int)(m.getHeight()*d_percent_boss);
		d_int_top=(int)((m.getWidth()-Main.int_left)*d_percent_top)-10;
		d_int_bottom=(int)((m.getHeight()-d_int_boss)*d_percent_bottom);
		
		right_width=m.getWidth()-Main.int_left-d_int_top-33;
		
		setLayout(new GridLayout(1,1));
		//上左部分
		p_topLeft =Defaults.panel(null,null);
		p_topLeft.setLayout(new BoxLayout(p_topLeft, BoxLayout.Y_AXIS));

		ta_chinese =new Area(true,null);	//中文文本域
		ta_english =new Area(false,null);	//英文文本域
		//添加文本域监听
		addAreaAdapter(ta_chinese,ta_english,CN);
		addAreaAdapter(ta_english,ta_chinese,EN);
		//添加文本域
		p_topLeft.add(Defaults.scroller(ta_chinese));
		p_topLeft.add(Defaults.scroller(ta_english));
		
		//上右部分（属性设置）
		p_topRight = Defaults.panel(null,"属性");
		p_topRight.setLayout(new FlowLayout(FlowLayout.LEFT,0,1));
			//映射-指代单选按钮
		l_refer=createLabel("映射",Defaults.offsetFeel, Color.black);
		p_group1 = createPanel(Defaults.defaultFeel, 30);
		p_group1.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
		
		rb_reference = Defaults.radio("指代",Defaults.defaultFeel);
		rb_reference.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				isCohesion=false;
			}
		});
		rb_cohesion = Defaults.radio("连接",Defaults.defaultFeel);
		rb_cohesion.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				isCohesion=true;
			}
		});
		rb_cohesion.setSelected(true);
		
		group1 = new ButtonGroup();
		group1.add(rb_reference);
		group1.add(rb_cohesion);
		
		p_group2 = createPanel(Color.LIGHT_GRAY, 30);
		p_group2.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
			//选择模式
		b_chooseModel=Defaults.button("一对一选择","switch", true);
		b_chooseModel.setPreferredSize(new Dimension(right_width, 30));
		b_chooseModel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//节点添加未完成不能切换选择模式，所以，若此处节点未添加，一定是自由模式的
				if(!one_to_one&&terminal!=null&&!preChain.contains(terminal)) {
					terminal.passed();
					return;//如果是自由选择结束，会调用重置按钮可用性方法，布朗值取反等操作不能执行
				}else
					one_to_one=!one_to_one;
				setChooseModelText();
			}
		});
			//省略
		l_ellipsis=createLabel("省略", Color.DARK_GRAY, Color.WHITE);
		tf_ellipsis=createField(Color.white, 30);
			
		rb_none=Defaults.radio("无",Color.LIGHT_GRAY);
		rb_none.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange()==ItemEvent.SELECTED) {
					Defaults.setFieldEnable(tf_ellipsis, false);
					ellipsis=NO;
					if(terminal==null||terminal.pass()) {//设置被选择时组件的状态
						ta_chinese.setEnabled(true);
						ta_english.setEnabled(true);
						ta_chinese.selectEllipLoc(false);
						ta_english.selectEllipLoc(false);
					}
					tf_ellipsis.setText("");
					recoverLast();
				}
			}
		});
		rb_ellipsis_cn=Defaults.radio("中文",Color.LIGHT_GRAY);
		rb_ellipsis_cn.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange()==ItemEvent.SELECTED) {
					Defaults.setFieldEnable(tf_ellipsis, true);
					ellipsis=CN;
					ta_chinese.selectEllipLoc(true);
					ta_english.selectEllipLoc(false);

					ta_chinese.setEnabled(true);
					ta_english.setEnabled(false);
					
					recoverLast();
				}
			}
		});
		rb_ellipsis_en=Defaults.radio("English",Color.LIGHT_GRAY);
		rb_ellipsis_en.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange()==ItemEvent.SELECTED) {
					Defaults.setFieldEnable(tf_ellipsis, true);
					ellipsis=EN;
					ta_english.selectEllipLoc(true);
					ta_chinese.selectEllipLoc(false);

					ta_english.setEnabled(true);
					ta_chinese.setEnabled(false);
					
					recoverLast();
				}
			}
		});
		
		rb_none.setSelected(true);
		group2=new ButtonGroup();
		group2.add(rb_none);
		group2.add(rb_ellipsis_cn);
		group2.add(rb_ellipsis_en);
		
		p_group1.add(rb_cohesion);
		p_group1.add(rb_reference);
		p_group2.add(rb_none);
		p_group2.add(rb_ellipsis_cn);
		p_group2.add(rb_ellipsis_en);
		
		b_attrSet=Defaults.button("属性设置","setting",true);
		b_attrSet.setPreferredSize(new Dimension(right_width,30));
		b_attrSet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AttrSetter();
			}
		});
		
		p_topRight.add(l_refer);
		p_topRight.add(p_group1);
		p_topRight.add(l_ellipsis);
		p_topRight.add(p_group2);
		p_topRight.add(tf_ellipsis);
		p_topRight.add(b_chooseModel);
		p_topRight.add(devider(Defaults.border_color));
		p_topRight.add(b_attrSet);
		
		Iterator<Attr> ia=Main.attrs.iterator();
		while(ia.hasNext()) {
			addAttrPanel(ia.next(),false);
		}
		resetP_RightHeight();
		
		sp_top=Defaults.spliter(JSplitPane.HORIZONTAL_SPLIT, d_int_top);
		Defaults.fillSpliter(sp_top, p_topLeft, Defaults.scroller(p_topRight));

		//下部分
		p_bottom =Defaults.panel(null,null);
		p_bottom.setLayout(new GridLayout(1,1));
		
		//创建底上部容器
		p_preChain = Defaults.panel(null,"当前链");
		p_preChain.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		p_preChain.setPreferredSize(new Dimension(m.getWidth()-Main.int_left-30,chain_preheight));
		
		//创建底下部容器
		p_linkOperator = Defaults.panel(null, "当前链操作");
		p_linkOperator.setLayout(new GridLayout(1,4,3,0));

		b_confirm = link_button("确定","add");
		b_confirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Left.addChain();
				terminal=null;
			}
		});
		b_revoke = link_button("撤销","reply");
		b_revoke.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				if(terminal.pass()){//如果是已经通过的节点，从链中删除
//					System.out.println("REVOKE A");
					removeNode(terminal);
					
//				}else{	//未通过的节点直接从视图删除
//					System.out.println("REVOKE B");
//					p_preChain.remove(terminal.getPlatform());
//					
//					ta_chinese.setEnabled(true);
//					ta_english.setEnabled(true);
//					Util.refreshComponent(p_preChain);
////					terminal=null;
//				}
				terminal=null;
				resetButtonEnable();
				
				//重置选择模式按钮
//				one_to_one=true;
//				b_chooseModel.setText("一对一选择");
//				b_revoke.setEnabled(false);
			}
		});
		
		b_clear = link_button("清空","clear");
		b_clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearChain();
			}
		});
		b_sort =link_button("排序（当前：中文）","cync");
		b_sort.setEnabled(true);
		b_sort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeSort();
				b_sort.setText(sortByCn?"排序（当前：中文）":"排序（当前：英文）");
				refillChain();
			}
		});
		
		
		p_linkOperator.add(b_confirm);
		p_linkOperator.add(b_revoke);
		p_linkOperator.add(b_clear);
		p_linkOperator.add(b_sort);
		
		
		sp_bottom=Defaults.spliter(JSplitPane.VERTICAL_SPLIT, d_int_bottom);
		sc_preChain=Defaults.scroller(p_preChain);
		sp_bottom.setLeftComponent(sc_preChain);
		sp_bottom.setRightComponent(p_linkOperator);
		
		sp_boss=Defaults.spliter(JSplitPane.VERTICAL_SPLIT,d_int_boss);//主分割面板
		sp_boss.setLeftComponent(sp_top);
		sp_boss.setRightComponent(sp_bottom);
		add(sp_boss);
		
		setAttrEnable();
	}

	/**
	 * 添加文本域监听
	 * @param listened	被监听文本域
	 * @param another	另一个文本域
	 * @param elliptype	省略符号
	 * FIXME 自由模式省略选择
	 */
	private void addAreaAdapter(final Area listened, final Area another, final int elliptype){
		listened.addMouseListener(new MouseAdapter() {
			int l_press;
			private boolean ellip=false;//是否省略
			public void mouseReleased(MouseEvent e) {
				if(e.getButton()==MouseEvent.BUTTON1&&!ellip) {		//未启用省略状态
					int l_release=listened.getCaretPosition();
					if(l_press==l_release)//error point:应该先考虑松开和按下的位置是否相同，相同则不考虑，避免双击自动选择文本的不可抗力
						return;
					
					String seleced=listened.getSelectedText();
					if(!Util.valid(seleced))
						return;	//选择字段不合法退出
					
					createOrFillNode(listened,another,Util.min(l_press,l_release),ellip);
				}
			}
			public void mousePressed(MouseEvent e) {
				if(e.getButton()==MouseEvent.BUTTON1)
					l_press=listened.getCaretPosition();//先获取点击位置
				else
					return;
				
				recoverLast();
				if(ellipsis!=NO&&ellipsis==elliptype) {//若有省略
					ellip=true;
					if(tf_ellipsis.getText().equals("")) {
						Defaults.message("请输入省略内容", Defaults.WARNING);//必须输入省略内容
						return;
					}
					
					createOrFillNode(listened, another, l_press,ellip);
					
					rb_none.setSelected(true);
					//中英之一添加结束，以下为恢复
					if(one_to_one) {
						rb_none.setEnabled(false);		//单选按钮不可用
						rb_ellipsis_en.setEnabled(false);
						rb_ellipsis_cn.setEnabled(false);
					}
					
					listened.selectEllipLoc(false);	//取消选择隐含位置
					tf_ellipsis.setEnabled(false);		//设置输入框不可用
					
					another.setEnabled(true);	//省略时另一个是不可用的，现在使其可用
					ellipsis=NO;		//省略标识已经使用过，此处恢复以防后患
					ellip=false;	//恢复无省略模式
				}
			}
		});
	}
	/**
	 * 创建或填充节点
	 * @param listened	被监听文本域
	 * @param another	另一个文本域
	 * @param loc	位置
	 */
	private void createOrFillNode(Area listened,Area another,int loc,boolean ellip){
		String text=ellip?tf_ellipsis.getText():listened.getSelectedText();
//		System.out.println(ellip+" "+text);
		//自由选择模式
		if(!one_to_one) {
//			System.out.println("CreateOrFillNode > freedom");
			
			if(terminal==null||preChain.contains(terminal)) {//terminal为空或当前链未添加terminal
				addTerminalNode(listened.forCn,text,loc);
				b_chooseModel.setEnabled(false);//不可切换选择模式
			}
			else
				terminal.freeSet(listened.forCn, text, loc);
			
			if(terminal.valid("both")) {
				terminal.setAttr(listened.forCn, false);
				b_chooseModel.setText("【点击选择结束】");
				b_chooseModel.setEnabled(true);//添加完成，开启切换选择模式按钮
			}
		}
		//一对一选择模式
		else if(terminal==null||terminal.pass()) {//创建新的节点
//			System.out.println("CreateOrFillNode > new node");
			addTerminalNode(listened.forCn, text,loc);
			listened.setEnabled(false);	//不可切换选择模式
			
			b_chooseModel.setEnabled(false);
		}else if((listened.forCn&&!terminal.validOf(true))||(!listened.forCn&&!terminal.validOf(false))){	//填充并添加节点
//			System.out.println("CreateOrFillNode > another");
			terminal.setAnother(text,loc,true);
			another.setEnabled(true);
			//节点添加完毕，恢复
			tf_ellipsis.setText("");	//节点添加完毕，清空省略输入框
			rb_none.setSelected(true);
			
			b_chooseModel.setEnabled(true);//添加完成，开启切换选择模式按钮
		}
		if(ellip)
			terminal.setEllipsisCell(listened.forCn);
	}
	//设置中英文
	public static void setParaChinese(String para_Cn) {
		ta_chinese.setText("");
		insert(ta_chinese, 0, para_Cn);
	}
	public static void setParaEnglish(String para_En) {
		ta_english.setText("");
		insert(ta_english, 0, para_En);
	}
	/**
	 * 创建和容器等长的标签
	 * @param text 文本
	 * @param bg 背景色
	 * @param fg 前景色
	 */
	private JLabel createLabel(String text,Color bg,Color fg) {
		JLabel l=Defaults.label(text,null,null);
		if(bg!=null) {
			l.setOpaque(true);
			l.setBackground(bg);
		}
		if(fg!=null)
			l.setForeground(fg);
		l.setPreferredSize(new Dimension(right_width,30));
		return l;
	}
	/**
	 * 创建和TOP_RIGHT等长的面板
	 * @param bg		背景色
	 * @param height	高度
	 */
	public static JPanel createPanel(Color bg,int height) {
		JPanel p=Defaults.panel(bg,null);
		p.setPreferredSize(new Dimension(right_width,height));
		return p;
	}
	/**
	 * 创建属性小面板
	 */
	private void addAttrPanel(Attr attr,boolean resetHeight) {
		attr.addToBearer(true);
		attr_sum++;
		
		if(resetHeight)		//重置右边版高度
			resetP_RightHeight();
		
	}
	/**
	 * 重置高度
	 */
	private void resetP_RightHeight() {
		int left=attr_sum-14;
		if(left>0)
			p_topRight.setPreferredSize(new Dimension(right_width,d_int_boss+10+left*33));
		else
			p_topRight.setPreferredSize(new Dimension(right_width,d_int_boss+10));
	}
	/**
	 * 创建分割条
	 * @param bg		背景色
	 */
	public static JPanel devider(Color bg) {
		return Defaults.devider(right_width, bg);
	}
	/**
	 * 创建文本域
	 * @param bg		背景色
	 * @param height	高度
	 */
	private JTextField createField(Color bg,int height) {
		JTextField f=Defaults.field("",bg,Color.BLACK);
		f.setPreferredSize(new Dimension(right_width,height));
		return f;
	}
	/**
	 * 创建操作链的按钮
	 * @param text	按钮文字
	 */
	private JButton link_button(String text,String icon) {
		JButton b=Defaults.button(text,icon,false);
		b.setPreferredSize(new Dimension(200,30));
		return b;
	}
	/**
	 * 添加末段临时的节点（未通过的新节点，放在最后，通过后节点会调用下面的方法）
	 */
	private void addTerminalNode(boolean cn_enter,String text,int loc) {
		terminal=new Node(cn_enter, text,XFile.getPrepage(),loc);
//		terminal.setCommonAttr(XFile.getPrepage(),ellipsis,0);//设置共同属性
		
		resetRBEnable();
		
		Left.setLast_n_NextEnable(false);
		p_preChain.add(terminal.getPlatform());	//添加节点平台
		Util.refreshComponent(p_preChain);
	}
	/**
	 * 添加到treeset、重新设置面板大小、重新布置面板（在node中通过后调用）
	 * @param node 通过后的节点
	 */
	public static void addNode(Node node) {
		preChain.add(node);	//添加到链中
		resetButtonEnable();
		refillChain();
		resetP_ChainHeight();
	}
	/**
	 * 移除节点
	 */
	public static void removeNode(Node node) {
		preChain.remove(node);
		resetButtonEnable();
		p_preChain.remove(node.getPlatform());
		resetP_ChainHeight();
		Util.refreshComponent(p_preChain);
	}
	/**
	 * 重置节点显示板高度
	 */
	public static void resetP_ChainHeight() {
		int needHeight=chain_baseheight*((preChain.size())/10+1);
		if(chain_preheight!=needHeight) {//重新布置链板
			chain_preheight=needHeight;
			p_preChain.setPreferredSize(new Dimension(Main.width-Main.int_left-30,chain_preheight));	
		}
		//设置滚动条到最大位置
		JScrollBar vb=sc_preChain.getVerticalScrollBar();
		if(vb!=null)
			vb.setValue(vb.getMaximum());
	}
	/**
	 * 切换排序
	 */
	public static void changeSort() {
		TreeSet<Node> sort=Defaults.treeset(sortByCn=!sortByCn);
		sort.addAll(preChain);
		preChain=sort;
	}
	/**
	 * 清空链
	 */
	public static void clearChain() {
		preChain.clear();
		p_preChain.removeAll();
		resetButtonEnable();
		Util.refreshComponent(p_preChain);
		
		terminal=null;
	}
	/**
	 * 重新添加节点
	 */
	public static void refillChain() {
		for(Node n:preChain) {
			p_preChain.remove(n.getPlatform());
			p_preChain.add(n.getPlatform());
		}
		Util.refreshComponent(p_preChain);
	}
	/**
	 * 显示单一框（不建议单独调用）
	 * @param isCn 是否是中文框
	 * @param node 显示的node
	 * @param viewloc 光标显示的位置
	 */
	public static void show(boolean isCn,Node node,Color color,boolean show) {
		TreeMap<Integer,String> map=node.getMap(isCn);
		Area area=isCn?ta_chinese:ta_english;
		if(node.validOf(isCn)) {
			int index=0;
			for(Integer i:map.keySet()) {
				int loc=holder?i:node.getPage_loc(isCn, i);//根据不同的情景返回段落位置或全文位置
				
				//FIXME 判断错误
//				System.out.println("Right.show > "+node.ellipsisOf(isCn, index)+" "+map.get(i));
				if(!node.ellipsisOf(isCn, index)) {		//不是省略或不要显示
					Defaults.modifyColor(color);
					modify(area,loc,map.get(i).length());
//					System.out.println(map.get(i));
				}else {
					Defaults.modifyColor(show?Color.blue:color);
					modify(area,loc-1,2);
				}
				index++;
			}
		}
		//设置光标位置
		int viewLoc=0;
		if(show) {
			if(holder)
				viewLoc=node.getLoc(isCn);
			else
				viewLoc=node.getPage_loc(isCn, node.getLoc(isCn));
				
		}
		area.setCaretPosition(viewLoc);
	}
	/**
	 * 根据不同的情景来恢复<br/>
	 * 在主窗体视图下，将恢复成黑色。在链显示视图下，将恢复成灰色。
	 */
	public static void recoverByScence(Node node){
		Color color=holder?recovery:ChainShower.sign;
//		if(holder)
//			Defaults.modifyColor(recovery);
//		else
//			Defaults.modifyColor(ChainShower.sign);
		show(true,node,color,false);
		show(false,node,color,false);
		node.preShowing=NO;
//		preShowingArea=NO;//当前无显示文本域
	}
	/**
	 * 恢复成黑色
	 */
	public static void recovery(Node node) {
		Defaults.modifyColor(recovery);
		show(true,node,recovery,false);
		show(false,node,recovery,false);
		node.preShowing=NO;
//		preShowingArea=NO;//当前无显示文本域
	}
	/**
	 * 恢复上一个显示的节点
	 */
	public static void recoverLast() {
		if(preShowing!=null)
			recoverByScence(preShowing);
		preShowing=null;
	}
	/**
	 * 改变指定位置的文字颜色
	 * @param from 	开始位置
	 * @param len	修改长度
	 */
	public static void modify(Area area,int from,int len) {
		StyledDocument doc=area.getDoc();
		if(from<0) {
			from=0;
			len--;
		}
		if(from+len>area.getText().length())
			len--;
		
		try {  
			String copy=doc.getText(from, len);
        	doc.remove(from,len);					//移除部分文字
            doc.insertString(from, copy, attr_set); //插入新格式的文字
        } catch (BadLocationException ex) {   
        	System.out.println("BadLocationException while modify from:"+from+" len:"+len);
        }
	}
	/**
	 * 移除文字，一般是隐含位置的删除
	 */
	public static void remove(Area area,int from,int len){
		StyledDocument doc=area.getDoc();
		try {
        	doc.remove(from,len);				//移除部分文字
        } catch (BadLocationException ex) {  
        	System.out.println("BadLocationException while removing from:"+from+" len:"+len);
        }  
	}
	/**
	 * 增加文字，一般是显示隐含位置时
	 */
	public static void insert(Area area,int from,String str){
		StyledDocument doc=area.getDoc();
		try {
            doc.insertString(from, str, attr_set); //插入新格式的文字
        } catch (BadLocationException ex) {  
        	System.out.println("BadLocationException while inserting from:"+from+" str:"+str);
        }  
	}
	/**
	 * 打开文件后的连锁反应
	 */
	public static void opened() {
		opened=true;
		resetRBEnable();//打开文件后重置单选按钮可用性
		setAttrEnable();
		p_topRight.setBackground(Defaults.defaultFeel);
		p_preChain.setBackground(Color.white);
	}
	/**
	 * 判断并重置按钮的可用性
	 */
	public static void resetButtonEnable(){
		if (preChain.size()>0) {
			b_confirm.setEnabled(true);
//			b_revoke.setEnabled(true);
			b_clear.setEnabled(true);
		}
		else {
			b_confirm.setEnabled(false);
//			b_revoke.setEnabled(false);
			b_clear.setEnabled(false);
		}
		b_revoke.setEnabled(terminal!=null);
		
		//重置选择模式按钮
		b_chooseModel.setEnabled(true);
		one_to_one=true;
		setChooseModelText();
	}
	/**
	 * 判断并重置单选按钮的可用性
	 */
	public static void resetRBEnable() {
		if(opened) {
			rb_cohesion.setEnabled(true);
			rb_reference.setEnabled(true);
			if(terminal==null||terminal.pass()||!one_to_one) {
				rb_none.setEnabled(true);
				rb_ellipsis_cn.setEnabled(true);
				rb_ellipsis_en.setEnabled(true);
			}else if(terminal.validOf(false))
				rb_ellipsis_en.setEnabled(false);
			else
				rb_ellipsis_cn.setEnabled(false);
		}
	}
	/**
	 * 重置属性选择版的可用性
	 */
	public static void setAttrEnable() {
		for(Attr a:Main.attrs) {
			a.getCombox().setEnabled(opened);
		}
	}
	/**
	 * 重置选择模式按钮的文字
	 */
	private static void setChooseModelText() {
		if(one_to_one)
			b_chooseModel.setText("一对一选择");
		else
			b_chooseModel.setText("自由选择");
	}
}
